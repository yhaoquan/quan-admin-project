-- 创建用户
CREATE USER `quan`@`%` IDENTIFIED WITH caching_sha2_password BY '123456';
-- 授权
GRANT All privileges ON *.* TO `quan`@`%`;
-- 创建数据库
CREATE DATABASE `quan-admin-project` CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_0900_ai_ci';
-- 进入数据库
use quan-admin-project

-- ------------------------------------------------------------
-- 以下是创建表和插入数据脚本
-- ------------------------------------------------------------
