<template>
    <div class="container">

        <div class="body_wrap">

            <el-card shadow="hover" ref="content_card">

                <!-- 基本搜索+功能按钮 -->
                <el-form ref="searchForm1" :inline="true" :model="searchForm" size="medium" label-width="">
                    <el-card class="table_tool_bar_wrap" shadow="never">
                        <div class="base_wrap">
                            <div class="left">
                                <el-form-item label="关键字搜索" prop="keyword">
                                    <el-input placeholder="" v-model="searchForm.keyword" clearable/>
                                </el-form-item>
                                <el-button @click="handleSearch" type="primary" size="medium">搜索</el-button>
                                <el-button @click="handleReset" type="info" plain size="medium">重置</el-button>
                                <el-button type="text" size="medium" @click="searchFlag = !searchFlag">
                                    高级搜索<i class="el-icon--right" :class="searchFlag?'el-icon-arrow-down':'el-icon-arrow-up'"></i>
                                </el-button>
                            </div>

                            <div class="right">
                                <el-button @click="handleAddOrUpdateVisible()" type="primary" size="medium" icon="el-icon-plus">新增</el-button>
                                <el-button @click="handleDelete()" type="danger" size="medium" icon="el-icon-delete" :disabled="selectionRows.length == 0">删除</el-button>
                            </div>
                        </div>
                    </el-card>
                </el-form>

                <!-- 高级搜索 -->
                <el-form ref="searchForm2" :inline="true" :model="searchForm" size="medium" label-width="120px">
                    <el-collapse-transition>
                        <el-card class="where_wrap" shadow="never" v-if="!searchFlag">
                            <el-row>
        #foreach($column in $columns)
                            #if($column.attrname != $pk.columnName)
                                <el-col :span="8">
                                    <el-form-item label="${column.comments}" prop="${column.attrname}">
                                        <el-input placeholder="" v-model="searchForm.${column.attrname}" clearable/>
                                    </el-form-item>
                                </el-col>
                            #end
        #end
                                <el-col :span="8">
                                    <el-form-item label="状态" prop="status">
                                        <el-select placeholder="" v-model="searchForm.status" clearable>
                                            <el-option label="正常" value="1"></el-option>
                                            <el-option label="禁用" value="2"></el-option>
                                        </el-select>
                                    </el-form-item>
                                </el-col>
                            </el-row>
                        </el-card>
                    </el-collapse-transition>
                </el-form>

                <!-- 表格工具栏 -->
                <div class="table_min_tools">
                    <TableTools
                        :columnList.sync="columnList"
                        @handleCheckAllChange="handleCheckAllChange"
                        @handleFullscreen="handleFullscreen('content_card')"
                        @handleReload="handleReload"
                        @handleStripe="handleStripe"/>
                </div>

                <!-- 表格 -->
                <el-table
                    ref="table"
                    size="medium"
                    height="500"
                    empty-text="暂无数据"
                    element-loading-text="给我一点时间"
                    fit border highlight-current-row
                    :stripe="stripe"
                    v-loading="loading"
                    :data="dataList"
                    @selection-change="handleSelection">

                    <el-table-column type="selection" width="55" prop="selection" v-if="isHideColumn('selection')" align="center"></el-table-column>
                    <el-table-column label="序号" type="index" prop="index" v-if="isHideColumn('index')" width="60" align="center" />
    #foreach($column in $columns)
                    #if($column.attrname != $pk.columnName)
                    <el-table-column label="${column.comments}" prop="${column.attrname}" v-if="isHideColumn('${column.attrname}')" align="left" show-overflow-tooltip/>
                    #end
    #end
                    <el-table-column label="状态" align="center" width="80px">
                        <template slot-scope="scope">
                            <el-tag type="success" size="small" v-if="scope.row.status === 1">正常</el-tag>
                            <el-tag type="danger" size="small" v-else>禁用</el-tag>
                        </template>
                    </el-table-column>

                    <el-table-column label="操作" prop="btn" v-if="isHideColumn('btn')" width="200px" align="left" fixed="right">
                        <template slot-scope="scope">
                            <el-button @click="handleAddOrUpdateVisible(scope.row.id)" type="text" size="small">编辑</el-button>
                            <el-button @click="handleDelete(scope.row)" type="text" size="small">删除</el-button>
                            <el-button @click="handleDrawerVisible(scope.row)" type="text" size="small">详情</el-button>
                        </template>
                    </el-table-column>

                </el-table>

                <!-- 分页 -->
                <TablePagination
                    :total.sync="total"
                    :page.sync="searchForm.page"
                    :limit.sync="searchForm.limit"
                    @handleSizeChange="handleSizeChange"
                    @handleCurrentChange="handleCurrentChange"/>
            </el-card>

            <!-- 弹窗, 新增 / 修改 -->
            <AddOrUpdate ref="addOrUpdate" @refreshDataList="getDataList"></AddOrUpdate>

            <!-- 右侧抽屉,详情信息 -->
            <el-drawer
                class="drawer_wrap"
                size="800px"
                :visible.sync="drawerVisible"
                :with-header="false"
                :modal="true"
                direction="rtl"
                :before-close="handleCloseDrawerDetail">
                <div class="header_title">{{detail.realname}}</div>

                <div class="header_body">
                    <el-form :model="detail" :inline="true" label-width="120">
                        <el-row>
                            <el-col :span="12">
                                <el-form-item label="姓名：">
                                    {{detail.realname}}
                                </el-form-item>
                            </el-col>
                            <el-col :span="12">
                                <el-form-item label="姓名：">
                                    {{detail.username}}
                                </el-form-item>
                            </el-col>
                        </el-row>
    #foreach($column in $columns)
                    #if($column.attrname != $pk.columnName)
                        <el-form-item label="${column.comments}：">
                            {{detail.${column.attrname}}}
                        </el-form-item>
                    #end
    #end
                    </el-form>
                </div>
            </el-drawer>

        </div>
    </div>
</template>

<script>
import TableMixin from '@/mixin/TableMixin'
import TableTools from '@/views/components/table/table-tools'
import TablePagination from '@/views/components/table/table-pagination'
import AddOrUpdate from './add-or-update-dialog'
export default {
    mixins: [TableMixin],
    components: {
        TableTools,
        TablePagination,
        AddOrUpdate
    },

    data() {
        return {
            dataList: [],
            total: 0,
            loading: false,
            drawerVisible: false,
            selectionRows: [],
            searchFlag: true,
            searchForm: {
                page: 1,
                limit: 10,
                sortColumns: null,
                sortType: null,
                keyword: null,
#foreach($column in $columns)
#if($column.columnName != $pk.columnName)
                ${column.attrname}: null#if($foreach.count < $columns.size()),#end
#end
#end
            },
            detail: {
#foreach($column in $columns)
                ${column.attrname}: null#if($foreach.count < $columns.size()),#end
#end
            }
        }
    },

    created() {
    },

    mounted() {
        this.$nextTick(() => {
            this.getDataList()
        })
    },

    methods: {

        /**
         * 侧边栏抽屉打开
         */
        handleDrawerVisible(row) {
            this.detail = row
            this.drawerVisible = true
        },

        /**
         * 新增或编辑
         */
        handleAddOrUpdateVisible(id) {
            // 表单页面
            // if(!id) {
            //     this.$router.push({name: 'user_form'})
            // } else {
            //     this.$router.push({name: 'user_form', query: {id: id}})
            // }

            // 表单窗口
            this.$nextTick(() => {
                this.$refs.addOrUpdate.init(id)
            })
        },

        /**
         * 删除
         */
        async handleDelete(row) {
            let ids = []
            if (row) {
                ids.push(row.id)
            }
            if(this.selectionRows) {
                this.selectionRows.filter((item, index) => {
                    ids.push(item.id)
                })
            }
            if (ids.length <= 0) {
                this.$notify({ title: '提示', message: '请选择需要删除的记录！', type: 'info' });
            } else {
                const confirm = await this.$confirm('确定要删除？', '提示', { type: 'warning' }).catch(() => {})
                if(confirm) {
                    const res = await this.$http({
                        url: '/${moduleName}/${pathName}/delete',
                        method: 'POST',
                        data: ids
                    })
                    if (this.ResultMessage(res)) {
                        this.getDataList()
                    }
                }
            }
        },

        /**
         * 获取表格数据
         */
        async getDataList() {
            this.loading = true
            const res = await this.$http({
                url: '/${moduleName}/${pathName}/page',
                method: 'GET',
                params: this.searchForm
            })
            if (this.ResultMessage(res, false)) {
                this.dataList = res.data.records || []
                this.total = res.data.total
            }
            this.loading = false
        },

        /**
         * 搜索
         */
        handleSearch() {
            this.getDataList()
        },
        /**
         * 重新加载数据表格
         */
        handleReload() {
            this.getDataList()
        },
        /**
         * 重置搜索表单
         */
        handleReset() {
            this.$refs.searchForm1.resetFields()
            this.$refs.searchForm2.resetFields()
        }

    }

}
</script>

<style lang="scss" scoped>

</style>
