package com.quan.system.init;

import com.quan.commons.core.constant.CommonsConstant;
import com.quan.commons.core.utils.OnlineUtils;
import com.quan.commons.core.utils.RedisUtils;
import com.quan.system.commons.vo.SysUserVo;
import com.quan.system.service.SysRolePermissionService;
import com.quan.system.service.SysUserRoleService;
import com.quan.system.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Set;

@Slf4j
@Component
public class StartUpOnlineUserByRedis implements CommandLineRunner {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public void run(String... args) throws Exception {
        log.info("服务启动初始化==>从Redis缓存中获取登录用户信息，加入到在线用户");
        final Set<String> keys = this.redisUtils.keys(CommonsConstant.RedisPrefix.SSO_TOKEN + ":*");
        for (String key : keys) {
            final String token = this.redisUtils.get(key);
            final SysUserVo userVo = this.sysUserService.getUserInfoByToken(token);
            OnlineUtils.setOnlineUserPools(userVo);
        }

    }
}
