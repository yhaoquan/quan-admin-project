package com.quan.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.IdType;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统-日志表
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-15 14:49:19
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("sys_log")
public class SysLog extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
    @TableId(type = IdType.AUTO)
    private Long id;

	/**
	 * 日志类型：1->登录/登出，2->业务，3->异常
	 */
    private Integer type;

	/**
	 * 操作用户
	 */
    private String username;

	/**
	 * IP地址
	 */
    private String remoteAddr;

	/**
	 * 日志标题
	 */
    private String title;

	/**
	 * 请求URI
	 */
    private String requestUri;

	/**
	 * 请求方式
	 */
    private String method;

	/**
	 * 请求数据
	 */
    private String params;

	/**
	 * 用户代理
	 */
    private String userAgent;

	/**
	 * 执行时间
	 */
    private Long time;

	/**
	 * 执行对象
	 */
	private String className;

	/**
	 * 执行方法
	 */
	private String methodName;

	/**
	 * 异常信息
	 */
    private String exception;

	/**
	 * 创建时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime ctime;

	/**
	 * 更新时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime utime;


}
