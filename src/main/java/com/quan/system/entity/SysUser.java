package com.quan.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统-用户
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class SysUser extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId
    private Long id;

	/**
	 * 用户姓名
	 */
    private String realname;

	/**
	 * 头像
	 */
    private String avatar;

	/**
	 * 登录账号
	 */
    @NotNull(message = "登录账号不能为空")
    private String username;

	/**
	 * 密码(SHA256(密码+盐))
	 */
    @NotNull(message = "密码(SHA256(密码+盐))不能为空")
    private String password;

	/**
	 * 盐
	 */
    @NotNull(message = "盐不能为空")
    private String salt;

	/**
	 * 手机号码
	 */
    private String mobile;

	/**
	 * 邮箱地址
	 */
    private String email;

	/**
	 * 性别(男,女)
	 */
    private String sex;

	/**
	 * 生日
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    private LocalDate birthday;

	/**
	 * 状态(0:禁用:,1:正常)
	 */
    private Integer status;

	/**
	 * 是否超级管理员（1：是，0：否）
	 */
    private Integer superAdmin;

	/**
	 * 创建时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime ctime;

	/**
	 * 最后更新时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime utime;


}
