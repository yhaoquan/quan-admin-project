package com.quan.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.IdType;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统-菜单
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-05 12:48:05
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class SysMenu extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId(type = IdType.AUTO)
    private Long id;

	/**
	 * PID
	 */
    private Long pid;

	/**
	 * 名称
	 */
    private String name;

	/**
	 * 路径
	 */
    private String url;

	/**
	 * 排序
	 */
    @NotNull(message = "排序不能为空")
    private Integer sort;

	/**
	 * 状态(0:禁用:,1:正常)
	 */
    private Integer status;

	/**
	 * 创建时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime ctime;

	/**
	 * 最后更新时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime utime;


}
