package com.quan.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.IdType;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统-定时作业管理
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("sys_scheduler_job")
public class SysSchedulerJob extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId(type = IdType.AUTO)
    private Long id;

	/**
	 * Job名称
	 */
    private String jobName;

	/**
	 * Job组名
	 */
    private String jobGroup;

	/**
	 * 执行的cron
	 */
    private String jobCron;

	/**
	 * 任务触发时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime jobTriggerDate;

	/**
	 * Job作业类
	 */
    private String jobClassPath;

	/**
	 * Job的参数
	 */
    private String jobParams;

	/**
	 * Job描述信息
	 */
    private String jobDesc;

	/**
	 * 任务执行状态，可选值为（RUNNING：正常-待触发执行中,  PAUSE：暂停, FINISHED：完成, FAILED：失败）
	 */
    private String jobStatus;

	/**
	 * 执行失败原因
	 */
    private String failureCause;

	/**
	 * 备注
	 */
    private String remarks;

	/**
	 * 创建时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime ctime;

	/**
	 * 最后更新时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime utime;


}
