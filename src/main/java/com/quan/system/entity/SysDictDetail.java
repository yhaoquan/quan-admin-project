package com.quan.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;


import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统-字典详情
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dict_detail")
public class SysDictDetail extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
    @TableId
    private Long id;

	/**
	 * 字典ID
	 */
    private Long dictId;

	/**
	 * 字典标签
	 */
    private String label;

	/**
	 * 字典值
	 */
    private String value;

	/**
	 * 设置默认选中(0：否，1：是)
	 */
    private Integer selected;

	/**
	 * 排序
	 */
    @NotNull(message = "排序不能为空")
    private Integer sort;

	/**
	 * 状态(0禁用:,1:正常)
	 */
    private Integer status;

	/**
	 * 备注
	 */
    private String remarks;

	/**
	 * 创建时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime ctime;

	/**
	 * 最后更新时间
	 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime utime;


}
