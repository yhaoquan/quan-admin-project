package com.quan.system.commons.vo;


import com.quan.system.entity.SysRole;
import com.quan.system.entity.SysUser;
import com.quan.system.entity.SysUserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 系统-用户
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SysUserVo extends SysUser {

    /**
     * 用户角色
     */
    private List<SysRole> roles;

    /**
     * 用户角色ID
     */
    private Long[] roleIds;

    /**
     * 原密码
     */
    private String oldPassword;

    /**
     * 新密码
     */
    private String newPassword;


}
