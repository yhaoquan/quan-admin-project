package com.quan.system.commons.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.quan.system.entity.SysSchedulerJob;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * 系统-定时作业管理
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SysSchedulerJobVo extends SysSchedulerJob {

    /**
     * 添加JobDataMap数据
     */
    @TableField(exist = false)
    private Map<String, Object> jobDataMap ;

}
