package com.quan.system.commons.vo;


import com.quan.system.entity.SysMenu;
import com.quan.system.entity.SysPermission;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 系统-菜单
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SysMenuVo extends SysMenu {

    private List<SysMenu> children;

    private List<SysPermission> permission;


}
