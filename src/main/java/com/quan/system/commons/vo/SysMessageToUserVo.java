package com.quan.system.commons.vo;


import com.quan.system.entity.SysMessageToUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 系统-即时消息-用户消息
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SysMessageToUserVo extends SysMessageToUser {


}
