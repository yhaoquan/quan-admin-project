package com.quan.system.commons.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * WebSocket 消息
 */
@Data
public class WebSocketMessageVo {

	/**
	 * 消息显示方式：Notification, MessageBox, Message
	 */
	private String mode ;

	/**
	 * 消息发送者(用户ID)
	 */
	private String from ;

	/**
	 * 消息发送者名称
	 */
	private String fromName ;

	/**
	 * 消息接收者
	 * 发送给多个用户
	 */
	private List<String> to;

	/**
	 * 发送的文本
	 */
	public String text;

	/**
	 * 消息发送时间
	 */
	public Date date = new Date();

	public WebSocketMessageVo() {
	}

	public WebSocketMessageVo(String from, List<String> to, String text) {
		this.from = from;
		this.to = to;
		this.text = text;
	}
}
