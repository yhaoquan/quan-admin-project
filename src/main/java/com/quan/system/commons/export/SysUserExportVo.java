package com.quan.system.commons.export;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.quan.commons.core.bean.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 系统-用户
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Data
@ToString(callSuper = true)
public class SysUserExportVo {

	/**
	 * ID
	 */
    private Long id;

	/**
	 * 用户姓名
	 */
    private String realname;

	/**
	 * 头像
	 */
    private String avatar;

	/**
	 * 登录账号
	 */
    private String username;

	/**
	 * 密码(SHA256(密码+盐))
	 */
    private String password;

	/**
	 * 盐
	 */
    private String salt;

	/**
	 * 手机号码
	 */
    private String mobile;

	/**
	 * 邮箱地址
	 */
    private String email;

	/**
	 * 性别(男,女)
	 */
    private String sex;

	/**
	 * 生日
	 */
    private LocalDate birthday;

	/**
	 * 状态(0:禁用:,1:正常)
	 */
    private Integer status;

	/**
	 * 是否超级管理员（1：是，0：否）
	 */
    private Integer superAdmin;

	/**
	 * 创建时间
	 */
    private LocalDateTime ctime;

	/**
	 * 最后更新时间
	 */
    private LocalDateTime utime;

}
