package com.quan.system.commons.job;

import com.alibaba.fastjson.JSON;
import com.quan.commons.core.utils.DateUtils;
import com.quan.system.commons.vo.WebSocketMessageVo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 发送即时广播消息
 */
@Slf4j
public class PushMsgToClientJob implements Job {

//    @Autowired
//    private SimpMessagingTemplate messagingTemplate ;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDetail jobDetail = context.getJobDetail();

        // 参数
//        Map<String, Object> params = new HashMap<String, Object>();
//
//        // JobDataMap数据
//        Set<Map.Entry<String, Object>> entrySet = jobDetail.getJobDataMap().entrySet();
//        for (Map.Entry<String, Object> entry : entrySet) {
//            params.put(entry.getKey(), entry.getValue());
//        }

//        WebSocketMessageVo message = new WebSocketMessageVo();
//        // 标识消息来自系统
//        message.setFrom("system");
//        message.setText("测试定时任务");
//        this.messagingTemplate.convertAndSend("/server/system/broadcast", JSON.toJSONString(message));

        log.info("==>【执行任务】广播即时消息：{}, {}, {}", DateUtils.getDateTime(), jobDetail.getKey(), jobDetail.getJobClass());
    }
}
