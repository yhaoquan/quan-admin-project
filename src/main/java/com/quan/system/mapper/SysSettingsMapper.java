package com.quan.system.mapper;

import com.quan.system.entity.SysSettings;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-全局常量设置表
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysSettingsMapper extends MyBaseMapper<SysSettings> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysSettings> selectMyPage(IPage<SysSettings> page, @Param("entity") SysSettings entity);

}
