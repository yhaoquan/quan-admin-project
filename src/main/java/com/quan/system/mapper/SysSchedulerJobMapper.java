package com.quan.system.mapper;

import com.quan.system.entity.SysSchedulerJob;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-定时作业管理
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysSchedulerJobMapper extends MyBaseMapper<SysSchedulerJob> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysSchedulerJob> selectMyPage(IPage<SysSchedulerJob> page, @Param("entity") SysSchedulerJob entity);

}
