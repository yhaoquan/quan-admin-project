package com.quan.system.mapper;

import com.quan.system.entity.SysFile;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-文件表
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysFileMapper extends MyBaseMapper<SysFile> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysFile> selectMyPage(IPage<SysFile> page, @Param("entity") SysFile entity);

}
