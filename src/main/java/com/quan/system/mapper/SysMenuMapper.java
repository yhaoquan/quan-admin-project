package com.quan.system.mapper;

import com.quan.system.entity.SysMenu;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-菜单
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysMenuMapper extends MyBaseMapper<SysMenu> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysMenu> selectMyPage(IPage<SysMenu> page, @Param("entity") SysMenu entity);

}
