package com.quan.system.mapper;

import com.quan.system.entity.SysMessageToUser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-即时消息-用户消息
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysMessageToUserMapper extends MyBaseMapper<SysMessageToUser> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysMessageToUser> selectMyPage(IPage<SysMessageToUser> page, @Param("entity") SysMessageToUser entity);

}
