package com.quan.system.mapper;

import com.quan.system.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统-角色与(菜单)权限关联
 * 
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysRolePermissionMapper extends MyBaseMapper<SysRolePermission> {

	/**
     * 分页查询列表
     * @param page
     * @param entity
     * @return
     */
    IPage<SysRolePermission> selectMyPage(IPage<SysRolePermission> page, @Param("entity") SysRolePermission entity);

}
