package com.quan.system.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.*;
import com.quan.system.commons.vo.SysUserVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import com.quan.system.commons.vo.SysSchedulerJobVo;
import com.quan.system.entity.SysSchedulerJob;
import com.quan.system.service.SysSchedulerJobService;


/**
 * 系统-定时作业管理
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-定时作业管理")
@RestController
@RequestMapping("/system/sysschedulerjob")
public class SysSchedulerJobController extends MyBaseController {

    @Autowired
    private SysSchedulerJobService service;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysSchedulerJobVo vo) throws SchedulerException, ClassNotFoundException {
        if(this.service.hasExist("job_name", vo.getJobName(), SysSchedulerJob.class)) {
            return R.failure("该值已存在");
        }
        if(null == vo.getJobGroup()) {
            vo.setJobGroup(Scheduler.DEFAULT_GROUP) ;
        }
        this.service.startJob(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysSchedulerJobVo vo) throws SchedulerException, ClassNotFoundException {
        this.service.deleteJobByName(vo.getJobName());

        this.service.updateById(vo);

        vo.setJobGroup(Scheduler.DEFAULT_GROUP) ;
        this.service.startJob(vo);

        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysSchedulerJobVo vo) {
        QueryWrapper<SysSchedulerJob> queryWrapper = new QueryWrapper<SysSchedulerJob>(vo);
        SysSchedulerJob sysSchedulerJob = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysSchedulerJob);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysSchedulerJob sysSchedulerJob = this.service.getById(id);
        return R.ok().put("data", sysSchedulerJob);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysSchedulerJobVo vo) {
        List<SysSchedulerJobVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysSchedulerJobVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

    /**
     *  暂停任务
     * @param vo
     * @return
     */
    @GetMapping(value = "/pauseJob")
    public Object pauseJob(SysSchedulerJobVo vo) throws SchedulerException {
        this.service.pauseJob(vo.getId(), vo.getJobName(), vo.getJobGroup());
        return R.ok();
    }

    /**
     * 恢复任务
     * @param vo
     * @return
     */
    @GetMapping(value = "/resumeJob")
    public Object resumeJob(SysSchedulerJobVo vo) throws SchedulerException {
        this.service.resumeJob(vo.getId(), vo.getJobName(), vo.getJobGroup());
        return R.ok();
    }

    /**
     * 触发任务
     * @param vo
     * @return
     */
    @GetMapping(value = "/triggerJob")
    public Object triggerJob(SysSchedulerJobVo vo) throws SchedulerException {
        this.service.triggerJob(vo.getId(), vo.getJobName(), vo.getJobGroup());
        return R.ok();
    }

}
