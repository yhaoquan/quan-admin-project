package com.quan.system.web.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.PageUtils;
import com.quan.system.commons.vo.SysLogVo;
import com.quan.system.entity.SysLog;
import com.quan.system.service.SysLogService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 系统-日志表
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-日志表")
@RestController
@RequestMapping("/system/syslog")
public class SysLogController extends MyBaseController {

    @Autowired
    private SysLogService service;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysLogVo vo) {
        this.service.save(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysLogVo vo) {
        this.service.updateById(vo);
        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysLogVo vo) {
        QueryWrapper<SysLog> queryWrapper = new QueryWrapper<SysLog>(vo);
        SysLog sysLog = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysLog);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysLog sysLog = this.service.getById(id);
        return R.ok().put("data", sysLog);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysLogVo vo) {
        List<SysLogVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysLogVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

    /**
     * 统计用户登录
     * 两小时一组，24小时
     * @param vo
     * @return
     */
    @GetMapping("/totalLoginReport")
    public R totalLoginReport(SysLogVo vo) {
        List<Integer> list = new ArrayList<Integer>();
        int k = 0;
        for (int i = 0; i < 12; i++) {
            QueryWrapper<SysLog> queryWrapper = new QueryWrapper<SysLog>(vo);
            queryWrapper.eq("type", 1);
            queryWrapper.apply("HOUR(ctime) >= {0} and HOUR(ctime) < {1}", k, k+2);
            // 当天的用户登录时间分布
            //queryWrapper.apply("date_format(ctime, '%Y-%m-%d') = {0} and HOUR(ctime) >= {1} and HOUR(ctime) < {2}", DateUtil.formatDate(new Date()), k, k+2);
            final int count = this.service.count(queryWrapper);
            list.add(count);
            k = k + 2;
        }
        return R.ok().put("data", list);
    }

}
