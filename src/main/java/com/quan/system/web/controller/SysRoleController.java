package com.quan.system.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.*;
import com.quan.system.commons.vo.SysRolePermissionVo;
import com.quan.system.commons.vo.SysUserVo;
import com.quan.system.entity.SysRolePermission;
import com.quan.system.service.SysRolePermissionService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.quan.system.commons.vo.SysRoleVo;
import com.quan.system.entity.SysRole;
import com.quan.system.service.SysRoleService;


/**
 * 系统-角色
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-角色")
@RestController
@RequestMapping("/system/sysrole")
public class SysRoleController extends MyBaseController {

    @Autowired
    private SysRoleService service;

    @Autowired
    private SysRolePermissionService sysRolePermissionService;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysRoleVo vo) {
        this.service.save(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysRoleVo vo) {
        this.service.updateById(vo);
        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysRoleVo vo) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<SysRole>(vo);
        SysRole sysRole = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysRole);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysRole sysRole = this.service.getById(id);
        return R.ok().put("data", sysRole);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysRoleVo vo) {
        List<SysRoleVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysRoleVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

    /**
     * 根据角色ID获取对应的权限
     * @param roleId
     * @return
     */
    @GetMapping(value = "/getPermissionByRoleId/{roleId}")
    public Object getPermissionByRoleId(@PathVariable Long roleId) {
        return R.ok().put("data", this.sysRolePermissionService.getPermissionByRoleId(roleId));
    }

    /**
     * 保存角对应的菜单权限
     * @param vo
     * @return
     */
    @PostMapping("/saveRolePermission")
    public Object saveRolePermission(@RequestBody SysRolePermissionVo vo) {

        //先把该角色对于的权限全部删除
        this.sysRolePermissionService.deleteByRoleId(vo.getRoleId());

        if(null != vo.getPermissions()) {
            List<SysRolePermission> list = new ArrayList<SysRolePermission>();

            for (String permission : vo.getPermissions()) {
                String[] split = permission.split("-") ;
                SysRolePermission entity = new SysRolePermission() ;
                entity.setRoleId(vo.getRoleId());
                entity.setMenuId(Long.parseLong(split[0]));
                entity.setPermissionId(Long.parseLong(split[1]));
                list.add(entity);
            }
            this.sysRolePermissionService.saveRolePermission(list);
            return R.ok();
        }
        return R.failure("保存角色对应的菜单权限失败");
    }
}
