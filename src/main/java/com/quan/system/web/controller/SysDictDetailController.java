package com.quan.system.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.*;
import com.quan.system.commons.vo.SysUserVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import com.quan.system.commons.vo.SysDictDetailVo;
import com.quan.system.entity.SysDictDetail;
import com.quan.system.service.SysDictDetailService;


/**
 * 系统-字典详情
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-字典详情")
@RestController
@RequestMapping("/system/sysdictdetail")
public class SysDictDetailController extends MyBaseController {

    @Autowired
    private SysDictDetailService service;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysDictDetailVo vo) {
        this.service.save(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysDictDetailVo vo) {
        this.service.updateById(vo);
        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysDictDetailVo vo) {
        QueryWrapper<SysDictDetail> queryWrapper = new QueryWrapper<SysDictDetail>(vo);
        SysDictDetail sysDictDetail = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysDictDetail);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysDictDetail sysDictDetail = this.service.getById(id);
        return R.ok().put("data", sysDictDetail);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysDictDetailVo vo) {
        List<SysDictDetailVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysDictDetailVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

}
