package com.quan.system.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.*;
import com.quan.system.commons.vo.SysUserVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import com.quan.system.commons.vo.SysMenuVo;
import com.quan.system.entity.SysMenu;
import com.quan.system.service.SysMenuService;


/**
 * 系统-菜单
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-菜单")
@RestController
@RequestMapping("/system/sysmenu")
public class SysMenuController extends MyBaseController {

    @Autowired
    private SysMenuService service;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysMenuVo vo) {
        this.service.save(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysMenuVo vo) {
        this.service.updateById(vo);
        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysMenuVo vo) {
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<SysMenu>(vo);
        SysMenu sysMenu = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysMenu);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id) {
        SysMenu sysMenu = this.service.getById(id);
        return R.ok().put("data", sysMenu);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysMenuVo vo) {
        List<SysMenuVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysMenuVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

    /**
     * 查询出所有分类以及子分类，以树形结构组装起来列表
     * @return
     */
    @GetMapping(value = "/listWithTree")
    public R listTree() {
        List<SysMenuVo> listWithTree = this.service.listWithTree();
        return R.ok().put("data", listWithTree);
    }

    /**
     * 查询所有，组装为树状结构，并加载对应的权限
     * @return
     */
    @GetMapping(value = "/listWithTreeOnPermission")
    public Object listWithTreeOnPermission() {
        return R.ok().put("data", this.service.listWithTreeOnPermission());
    }

}
