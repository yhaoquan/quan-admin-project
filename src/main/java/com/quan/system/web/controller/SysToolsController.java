package com.quan.system.web.controller;

import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.tools.mail.MailTools;
import com.quan.commons.tools.mail.vo.MailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * 系统工具
 */
@Slf4j
@RestController
@RequestMapping("/system/tools")
public class SysToolsController extends MyBaseController {

    @Autowired
    private MailTools mailTools;


    /**
     * 发送邮件
     * @param vo
     * @return
     */
    @PostMapping("sendMail")
    public R sendMail(@Valid @RequestBody MailVo vo) {
        this.mailTools.send(vo);

        return R.ok();
    }

}
