package com.quan.system.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.quan.commons.core.bean.R;
import com.quan.commons.core.biz.support.MyBaseController;
import com.quan.commons.core.utils.*;
import com.quan.system.commons.vo.SysUserVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import com.quan.system.commons.vo.SysMessageToUserVo;
import com.quan.system.entity.SysMessageToUser;
import com.quan.system.service.SysMessageToUserService;


/**
 * 系统-即时消息-用户消息
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
@Slf4j
@Api(tags = "系统-即时消息-用户消息")
@RestController
@RequestMapping("/system/sysmessagetouser")
public class SysMessageToUserController extends MyBaseController {

    @Autowired
    private SysMessageToUserService service;

    /**
     * 保存
     * @param vo
     * @return
     */
    @PostMapping("save")
    public R save(@Valid @RequestBody SysMessageToUserVo vo) {
        this.service.save(vo);
        return R.ok();
    }

    /**
     * 修改
     * @param vo
     * @return
     */
    @PostMapping("update")
    public R update(@Valid @RequestBody SysMessageToUserVo vo) {
        this.service.updateById(vo);
        return R.ok();
    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @PostMapping("delete")
    public R delete(@RequestBody Long[] ids) {
        if (null != ids && ids.length > 0) {
            this.service.removeByIds(Arrays.asList(ids));
            return R.ok();
        } else {
            return R.failure();
        }
    }

    /**
     * 多条件查询信息详情
     * @param vo
     * @return
     */
    @GetMapping("/info")
    public R info(SysMessageToUserVo vo) {
        QueryWrapper<SysMessageToUser> queryWrapper = new QueryWrapper<SysMessageToUser>(vo);
        SysMessageToUser sysMessageToUser = this.service.getOne(queryWrapper);
        return R.ok().put("data", sysMessageToUser);
    }

    /**
     * 根据ID查询信息详情
     * @param {id}
     * @return
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        SysMessageToUser sysMessageToUser = this.service.getById(id);
        return R.ok().put("data", sysMessageToUser);
    }

    /**
     * 列表查询
     * @param vo
     * @return
     */
    @GetMapping(value = "list")
    public Object list(SysMessageToUserVo vo) {
        List<SysMessageToUserVo> list = this.service.queryList(new PageUtils(request), vo);
        return R.ok().put("data", list);
    }

    /**
     * 分页查询
     * @param vo
     * @return
     */
    @GetMapping(value = "page")
    public R queryPage(SysMessageToUserVo vo) {
        PageUtils page = this.service.queryPage(new PageUtils(request), vo);
        return R.ok().put("data", page);
    }

}
