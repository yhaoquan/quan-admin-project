package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysUserRoleVo;
import com.quan.system.entity.SysUserRole;
import com.quan.system.mapper.SysUserRoleMapper;
import com.quan.system.service.SysUserRoleService;


@Slf4j
@Service
public class SysUserRoleServiceImpl extends MyBaseServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Autowired
	private SysUserRoleMapper mapper;

	@Override
    public List<SysUserRoleVo> queryList(PageUtils pageUtils, SysUserRoleVo vo) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<SysUserRole>(vo);

        List<SysUserRole> list = super.list(pageUtils, queryWrapper);
        List<SysUserRoleVo> collect = list.stream().map(item -> {
            SysUserRoleVo bean = new SysUserRoleVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysUserRoleVo vo) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<SysUserRole>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysUserRole> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysUserRole> records = page.getRecords();

        final List<SysUserRole> collect = records.stream().map((item) -> {
            SysUserRoleVo bean = new SysUserRoleVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }

    @Override
    public void deleteByUserId(Serializable userId) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<SysUserRole>();
        queryWrapper.eq("user_id", userId);
        super.remove(queryWrapper) ;
    }

    @Override
    public List<SysUserRole> getRolesByUserId(Serializable userId) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<SysUserRole>();
        queryWrapper.eq("user_id", userId);
        return super.list(queryWrapper);
    }

    @Override
    public List<SysUserRole> getRoleIdsByUserId(Serializable userId) {
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<SysUserRole>();
        queryWrapper.eq("user_id", userId).select("role_id");
        return super.list(queryWrapper);
    }
}