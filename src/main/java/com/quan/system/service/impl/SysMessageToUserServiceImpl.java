package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysMessageToUserVo;
import com.quan.system.entity.SysMessageToUser;
import com.quan.system.mapper.SysMessageToUserMapper;
import com.quan.system.service.SysMessageToUserService;


@Slf4j
@Service
public class SysMessageToUserServiceImpl extends MyBaseServiceImpl<SysMessageToUserMapper, SysMessageToUser> implements SysMessageToUserService {

    @Autowired
	private SysMessageToUserMapper mapper;

	@Override
    public List<SysMessageToUserVo> queryList(PageUtils pageUtils, SysMessageToUserVo vo) {
        QueryWrapper<SysMessageToUser> queryWrapper = new QueryWrapper<SysMessageToUser>(vo);

        List<SysMessageToUser> list = super.list(pageUtils, queryWrapper);
        List<SysMessageToUserVo> collect = list.stream().map(item -> {
            SysMessageToUserVo bean = new SysMessageToUserVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysMessageToUserVo vo) {
        QueryWrapper<SysMessageToUser> queryWrapper = new QueryWrapper<SysMessageToUser>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysMessageToUser> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysMessageToUser> records = page.getRecords();

        final List<SysMessageToUser> collect = records.stream().map((item) -> {
            SysMessageToUserVo bean = new SysMessageToUserVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }
}