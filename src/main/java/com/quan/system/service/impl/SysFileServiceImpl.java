package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysFileVo;
import com.quan.system.entity.SysFile;
import com.quan.system.mapper.SysFileMapper;
import com.quan.system.service.SysFileService;


@Slf4j
@Service
public class SysFileServiceImpl extends MyBaseServiceImpl<SysFileMapper, SysFile> implements SysFileService {

    @Autowired
	private SysFileMapper mapper;

	@Override
    public List<SysFileVo> queryList(PageUtils pageUtils, SysFileVo vo) {
        QueryWrapper<SysFile> queryWrapper = new QueryWrapper<SysFile>(vo);

        List<SysFile> list = super.list(pageUtils, queryWrapper);
        List<SysFileVo> collect = list.stream().map(item -> {
            SysFileVo bean = new SysFileVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysFileVo vo) {
        QueryWrapper<SysFile> queryWrapper = new QueryWrapper<SysFile>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysFile> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysFile> records = page.getRecords();

        final List<SysFile> collect = records.stream().map((item) -> {
            SysFileVo bean = new SysFileVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }
}