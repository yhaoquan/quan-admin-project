package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysRolePermissionVo;
import com.quan.system.entity.SysRolePermission;
import com.quan.system.mapper.SysRolePermissionMapper;
import com.quan.system.service.SysRolePermissionService;


@Slf4j
@Service
public class SysRolePermissionServiceImpl extends MyBaseServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {

    @Autowired
	private SysRolePermissionMapper mapper;

	@Override
    public List<SysRolePermissionVo> queryList(PageUtils pageUtils, SysRolePermissionVo vo) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>(vo);

        List<SysRolePermission> list = super.list(pageUtils, queryWrapper);
        List<SysRolePermissionVo> collect = list.stream().map(item -> {
            SysRolePermissionVo bean = new SysRolePermissionVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysRolePermissionVo vo) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysRolePermission> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysRolePermission> records = page.getRecords();

        final List<SysRolePermission> collect = records.stream().map((item) -> {
            SysRolePermissionVo bean = new SysRolePermissionVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }

    @Override
    public void deleteByRoleId(Serializable roleId) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>();
        queryWrapper.eq("role_id", roleId);

        super.remove(queryWrapper) ;
    }

    @Override
    public void deleteByMenuId(Serializable menuId) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>();
        queryWrapper.eq("menu_id", menuId);
        super.remove(queryWrapper) ;
    }

    @Override
    public void saveRolePermission(List<SysRolePermission> permissions) {
        super.saveBatch(permissions);
    }

    @Override
    public List<SysRolePermission> getPermissionByRoleId(Long roleId) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>();
        queryWrapper.eq("role_id", roleId).select("permission_id", "menu_id");
        return super.list(queryWrapper);
    }

    @Override
    public List<SysRolePermission> getPermissionByRoleIds(List<Long> roleIds) {
        QueryWrapper<SysRolePermission> queryWrapper = new QueryWrapper<SysRolePermission>();
        queryWrapper.in("role_id", roleIds).select("permission_id", "menu_id");
        return super.list(queryWrapper);
    }
}