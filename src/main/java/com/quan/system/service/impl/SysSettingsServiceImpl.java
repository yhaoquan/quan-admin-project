package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysSettingsVo;
import com.quan.system.entity.SysSettings;
import com.quan.system.mapper.SysSettingsMapper;
import com.quan.system.service.SysSettingsService;


@Slf4j
@Service
public class SysSettingsServiceImpl extends MyBaseServiceImpl<SysSettingsMapper, SysSettings> implements SysSettingsService {

    @Autowired
	private SysSettingsMapper mapper;

	@Override
    public List<SysSettingsVo> queryList(PageUtils pageUtils, SysSettingsVo vo) {
        QueryWrapper<SysSettings> queryWrapper = new QueryWrapper<SysSettings>(vo);

        List<SysSettings> list = super.list(pageUtils, queryWrapper);
        List<SysSettingsVo> collect = list.stream().map(item -> {
            SysSettingsVo bean = new SysSettingsVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysSettingsVo vo) {
        QueryWrapper<SysSettings> queryWrapper = new QueryWrapper<SysSettings>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysSettings> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysSettings> records = page.getRecords();

        final List<SysSettings> collect = records.stream().map((item) -> {
            SysSettingsVo bean = new SysSettingsVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }
}