package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysDictVo;
import com.quan.system.entity.SysDict;
import com.quan.system.mapper.SysDictMapper;
import com.quan.system.service.SysDictService;


@Slf4j
@Service
public class SysDictServiceImpl extends MyBaseServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    @Autowired
	private SysDictMapper mapper;

	@Override
    public List<SysDictVo> queryList(PageUtils pageUtils, SysDictVo vo) {
        QueryWrapper<SysDict> queryWrapper = new QueryWrapper<SysDict>(vo);

        List<SysDict> list = super.list(pageUtils, queryWrapper);
        List<SysDictVo> collect = list.stream().map(item -> {
            SysDictVo bean = new SysDictVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysDictVo vo) {
        QueryWrapper<SysDict> queryWrapper = new QueryWrapper<SysDict>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysDict> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysDict> records = page.getRecords();

        final List<SysDict> collect = records.stream().map((item) -> {
            SysDictVo bean = new SysDictVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }
}