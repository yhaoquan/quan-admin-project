package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysPermissionVo;
import com.quan.system.entity.SysPermission;
import com.quan.system.mapper.SysPermissionMapper;
import com.quan.system.service.SysPermissionService;


@Slf4j
@Service
public class SysPermissionServiceImpl extends MyBaseServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {

    @Autowired
	private SysPermissionMapper mapper;

	@Override
    public List<SysPermissionVo> queryList(PageUtils pageUtils, SysPermissionVo vo) {
        QueryWrapper<SysPermission> queryWrapper = new QueryWrapper<SysPermission>(vo);

        List<SysPermission> list = super.list(pageUtils, queryWrapper);
        List<SysPermissionVo> collect = list.stream().map(item -> {
            SysPermissionVo bean = new SysPermissionVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysPermissionVo vo) {
        QueryWrapper<SysPermission> queryWrapper = new QueryWrapper<SysPermission>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysPermission> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysPermission> records = page.getRecords();

        final List<SysPermission> collect = records.stream().map((item) -> {
            SysPermissionVo bean = new SysPermissionVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }

    @Override
    public List<SysPermission> findPermissionByMenuId(Serializable menuId) {

        QueryWrapper<SysPermission> queryWrapper = new QueryWrapper<SysPermission>();
        queryWrapper.eq("menu_id", menuId);

        return super.list(queryWrapper);
    }

    @Override
    public boolean deleteByMenuId(Serializable menuId) {
        QueryWrapper<SysPermission> wrapper = new QueryWrapper<SysPermission>();
        wrapper.eq("menu_id", menuId);
        return super.remove(wrapper);
    }
}