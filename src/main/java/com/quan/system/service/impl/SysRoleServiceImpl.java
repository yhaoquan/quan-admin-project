package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import com.quan.system.service.SysRolePermissionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysRoleVo;
import com.quan.system.entity.SysRole;
import com.quan.system.mapper.SysRoleMapper;
import com.quan.system.service.SysRoleService;


@Slf4j
@Service
public class SysRoleServiceImpl extends MyBaseServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
	private SysRoleMapper mapper;

    @Autowired
    private SysRolePermissionService rolePermissionService;

    @Override
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        boolean falg = super.removeByIds(idList);

        idList.forEach(id -> {
            this.rolePermissionService.deleteByRoleId(id);
        });

        return falg;
    }

	@Override
    public List<SysRoleVo> queryList(PageUtils pageUtils, SysRoleVo vo) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<SysRole>(vo);

        List<SysRole> list = super.list(pageUtils, queryWrapper);
        List<SysRoleVo> collect = list.stream().map(item -> {
            SysRoleVo bean = new SysRoleVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysRoleVo vo) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<SysRole>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysRole> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysRole> records = page.getRecords();

        final List<SysRole> collect = records.stream().map((item) -> {
            SysRoleVo bean = new SysRoleVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }

    @Override
    public List<SysRole> getRoles(List<Long> roleIds) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<SysRole>();
        queryWrapper.in("id", roleIds);
        return super.list(queryWrapper);
    }
}