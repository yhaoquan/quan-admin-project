package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.constant.CommonsConstant;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.utils.SchedulerUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysSchedulerJobVo;
import com.quan.system.entity.SysSchedulerJob;
import com.quan.system.mapper.SysSchedulerJobMapper;
import com.quan.system.service.SysSchedulerJobService;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
public class SysSchedulerJobServiceImpl extends MyBaseServiceImpl<SysSchedulerJobMapper, SysSchedulerJob> implements SysSchedulerJobService {

    @Autowired
	private SysSchedulerJobMapper mapper;

	@Override
    public List<SysSchedulerJobVo> queryList(PageUtils pageUtils, SysSchedulerJobVo vo) {
        QueryWrapper<SysSchedulerJob> queryWrapper = new QueryWrapper<SysSchedulerJob>(vo);

        List<SysSchedulerJob> list = super.list(pageUtils, queryWrapper);
        List<SysSchedulerJobVo> collect = list.stream().map(item -> {
            SysSchedulerJobVo bean = new SysSchedulerJobVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysSchedulerJobVo vo) {
        QueryWrapper<SysSchedulerJob> queryWrapper = new QueryWrapper<SysSchedulerJob>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("job_name", vo.getKeyword());
            });
        }

        final IPage<SysSchedulerJob> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysSchedulerJob> records = page.getRecords();

        final List<SysSchedulerJob> collect = records.stream().map((item) -> {
            SysSchedulerJobVo bean = new SysSchedulerJobVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }

    @Autowired
    private SchedulerUtils schedulerUtils;

    @Override
    public boolean startJob(SysSchedulerJobVo entity) throws ClassNotFoundException, SchedulerException {
        if (StringUtils.isBlank(entity.getJobStatus())) {
            entity.setJobStatus(CommonsConstant.SchedulerStatus.RUNNING.getName());
        }
        if (StringUtils.isBlank(entity.getJobGroup())) {
            entity.setJobGroup(Scheduler.DEFAULT_GROUP);
        }

        // 先把旧的定时任务删除
        QueryWrapper<SysSchedulerJob> queryWrapper = new QueryWrapper<SysSchedulerJob>();
        queryWrapper.eq("job_name", entity.getJobName()) ;
        List<SysSchedulerJob> list = super.list(queryWrapper);
        if(null != list && !list.isEmpty()) {
            list.forEach(item->{
                try {
                    this.schedulerUtils.deleteJob(item.getJobName(), item.getJobGroup());
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
                super.removeById(item.getId());
            });
        }

        boolean flag = super.save(entity);
        if (flag) {
            // 新建定时任务
            this.schedulerUtils.newJob(entity);

            // 任务为暂停状态
            if (entity.getJobStatus().equals(CommonsConstant.SchedulerStatus.PAUSE.getName())) {
                this.pauseJob(entity.getId(), entity.getJobName(), entity.getJobGroup());
            }
        }

        return flag;
    }

    @Override
    public boolean removeByIds(Collection<? extends Serializable> idList) {
        // 先把定时任务删除
        Collection<SysSchedulerJob> list = super.listByIds(idList);
        list.forEach(item -> {
            try {
                this.schedulerUtils.deleteJob(item.getJobName(), item.getJobGroup());
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        });

        return super.removeByIds(idList);
    }

    public boolean updateJob(SysSchedulerJobVo vo) throws ClassNotFoundException, SchedulerException {
        // 先把旧的定时任务删除
        SysSchedulerJob oldJob = super.getById(vo.getId());
        this.schedulerUtils.deleteJob(oldJob.getJobName(), oldJob.getJobGroup());

        boolean flag = super.updateById(vo);
        if (flag) {
            // 重新新建定时任务
            this.schedulerUtils.newJob(vo);
        }

        return super.updateById(vo);
    }

    @Override
    public void deleteJobByName(String jobName) throws SchedulerException {
        this.deleteJobByName(jobName, Scheduler.DEFAULT_GROUP);
    }

    @Override
    public void deleteJobByName(String jobName, String jobGroup) {
        QueryWrapper<SysSchedulerJob> queryWrapper = new QueryWrapper<SysSchedulerJob>();
        queryWrapper.eq("job_name", jobName).eq("job_group", jobGroup);
        List<SysSchedulerJob> list = super.list(queryWrapper) ;

        if(null != list && !list.isEmpty()) {
            List<Serializable> delIds = new ArrayList<Serializable>();
            list.forEach(item -> {
                delIds.add(item.getId());
                try {
                    this.schedulerUtils.deleteJob(item.getJobName(), item.getJobGroup());
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            });
            super.removeByIds(delIds);
        }

    }

    @Transactional
    @Override
    public void pauseJob(Long id, String jobName, String jobGroup) throws SchedulerException {
        this.schedulerUtils.pauseJob(jobName, jobGroup);
        this.changeStatus(id, CommonsConstant.SchedulerStatus.PAUSE.getName());
    }

    @Transactional
    @Override
    public void resumeJob(Long id, String jobName, String jobGroup) throws SchedulerException {
        this.schedulerUtils.resumeJob(jobName, jobGroup);
        this.changeStatus(id, CommonsConstant.SchedulerStatus.RUNNING.getName());
    }

    @Override
    public void triggerJob(Long id, String jobName, String jobGroup) throws SchedulerException {
        this.schedulerUtils.triggerJob(jobName, jobGroup);
    }

    @Override
    public void changeStatus(Long id, String jobStatus) {
        SysSchedulerJob entity = new SysSchedulerJob();
        entity.setId(id);
        entity.setJobStatus(jobStatus);
        super.updateById(entity);
    }
}