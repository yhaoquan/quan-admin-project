package com.quan.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.quan.commons.core.biz.support.MyBaseServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.quan.system.commons.vo.SysDictDetailVo;
import com.quan.system.entity.SysDictDetail;
import com.quan.system.mapper.SysDictDetailMapper;
import com.quan.system.service.SysDictDetailService;


@Slf4j
@Service
public class SysDictDetailServiceImpl extends MyBaseServiceImpl<SysDictDetailMapper, SysDictDetail> implements SysDictDetailService {

    @Autowired
	private SysDictDetailMapper mapper;

	@Override
    public List<SysDictDetailVo> queryList(PageUtils pageUtils, SysDictDetailVo vo) {
        QueryWrapper<SysDictDetail> queryWrapper = new QueryWrapper<SysDictDetail>(vo);

        List<SysDictDetail> list = super.list(pageUtils, queryWrapper);
        List<SysDictDetailVo> collect = list.stream().map(item -> {
            SysDictDetailVo bean = new SysDictDetailVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public PageUtils queryPage(PageUtils pageUtils, SysDictDetailVo vo) {
        QueryWrapper<SysDictDetail> queryWrapper = new QueryWrapper<SysDictDetail>(vo);

        if(StringUtils.isNotBlank(vo.getKeyword())) {
            queryWrapper.and(w -> {
                w.like("username", vo.getKeyword());
            });
        }

        final IPage<SysDictDetail> page = super.selectPage(pageUtils, queryWrapper);

        PageUtils ps = new PageUtils(page);

        final List<SysDictDetail> records = page.getRecords();

        final List<SysDictDetail> collect = records.stream().map((item) -> {
            SysDictDetailVo bean = new SysDictDetailVo();
            BeanUtils.copyProperties(item, bean);
            return bean;
        }).collect(Collectors.toList());

        ps.setRecords(collect);
        return ps;
    }
}