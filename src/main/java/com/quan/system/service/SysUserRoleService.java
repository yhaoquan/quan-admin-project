package com.quan.system.service;

import com.quan.system.commons.vo.SysUserRoleVo;
import com.quan.system.entity.SysUserRole;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 系统-用户与角色关联
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysUserRoleService extends MyBaseService<SysUserRole> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysUserRoleVo> queryList(PageUtils pageUtils, SysUserRoleVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysUserRoleVo vo);

    /**
     * 根据用户ID删除角色
     * @param userId
     */
    public void deleteByUserId(Serializable userId) ;

    /**
     * 根据用户ID获取角色关联关系数据
     * @param userId
     * @return
     */
    public List<SysUserRole> getRolesByUserId(Serializable userId) ;

    /**
     * 根据用户ID获取角色关联关系数据
     * @param userId
     * @return 角色ID
     */
    public List<SysUserRole> getRoleIdsByUserId(Serializable userId) ;
}

