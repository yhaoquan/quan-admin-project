package com.quan.system.service;

import com.quan.system.commons.vo.SysLogVo;
import com.quan.system.entity.SysLog;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-日志表
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysLogService extends MyBaseService<SysLog> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysLogVo> queryList(PageUtils pageUtils, SysLogVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysLogVo vo);
}

