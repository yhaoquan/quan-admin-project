package com.quan.system.service;

import com.quan.system.commons.vo.SysRoleVo;
import com.quan.system.entity.SysRole;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-角色
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysRoleService extends MyBaseService<SysRole> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysRoleVo> queryList(PageUtils pageUtils, SysRoleVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysRoleVo vo);

    /**
     * 获取角色
     * @param roleIds
     * @return
     */
    public List<SysRole> getRoles(List<Long> roleIds) ;
}

