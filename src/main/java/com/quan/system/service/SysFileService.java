package com.quan.system.service;

import com.quan.system.commons.vo.SysFileVo;
import com.quan.system.entity.SysFile;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-文件表
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysFileService extends MyBaseService<SysFile> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysFileVo> queryList(PageUtils pageUtils, SysFileVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysFileVo vo);
}

