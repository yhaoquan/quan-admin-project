package com.quan.system.service;

import com.quan.system.commons.vo.SysUserVo;
import com.quan.system.entity.SysPermission;
import com.quan.system.entity.SysRole;
import com.quan.system.entity.SysRolePermission;
import com.quan.system.entity.SysUser;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-用户
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysUserService extends MyBaseService<SysUser> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysUserVo> queryList(PageUtils pageUtils, SysUserVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysUserVo vo);

    /**
     * 修改用户
     * @param vo
     */
    void updateUser(SysUserVo vo);

    /**
     * 添加用户
     * @param vo
     */
    void saveUser(SysUserVo vo);

    /**
     * 根据用户名查询
     * @param username
     * @return
     */
    public SysUser getUser(String username);

    /**
     * 用户登录
     * @param vo
     * @return
     */
    public String login(SysUserVo vo);

    /**
     * 根据token获取用户信息
     * @param token
     * @return
     */
    public SysUserVo getUserInfoByToken(String token);

    /**
     * 获取用户的角色
     * @param userId
     * @return
     */
    public List<SysRole> getUserRoles(Long userId);

    /**
     * 获取用户的权限
     * @param userId
     * @return
     */
    public List<SysPermission> getUserPermissions(Long userId);

    /**
     * 获取用户权限关联关系信息
     * @param roles
     * @return
     */
    public List<SysRolePermission> getUserPermissionsRelation(List<SysRole> roles);

    /**
     * 验证字段值是否存在
     * @param field
     * @param value
     * @return
     */
    public boolean hasFieldValue(String field, Object value);

    /**
     * 修改数据，验证字段值是否存在，排除自己
     * @param field
     * @param value
     * @return
     */
    public boolean hasFieldValueForUpdate(Long id, String field, Object value);

    /**
     * 修改个人登录密码
     *
     * @param id          用户ID
     * @param oldPassword 原密码
     * @param newPassword 新密码
     */
    public void updatePassword(Long id, String oldPassword, String newPassword);

    /**
     * 重置用户登陆密码为[123456]
     * @param idList 用户ID集合
     */
    public boolean batchResetPassword(List<Long> idList) ;

    /**
     * 修改状态
     * @param id
     * @param status
     */
    void changeStatus(Long id, Integer status);
}

