package com.quan.system.service;

import com.quan.system.commons.vo.SysMessageToUserVo;
import com.quan.system.entity.SysMessageToUser;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-即时消息-用户消息
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysMessageToUserService extends MyBaseService<SysMessageToUser> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysMessageToUserVo> queryList(PageUtils pageUtils, SysMessageToUserVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysMessageToUserVo vo);
}

