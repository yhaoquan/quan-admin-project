package com.quan.system.service;

import com.quan.system.commons.vo.SysSettingsVo;
import com.quan.system.entity.SysSettings;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-全局常量设置表
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysSettingsService extends MyBaseService<SysSettings> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysSettingsVo> queryList(PageUtils pageUtils, SysSettingsVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysSettingsVo vo);
}

