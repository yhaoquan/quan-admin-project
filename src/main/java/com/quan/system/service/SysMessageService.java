package com.quan.system.service;

import com.quan.system.commons.vo.SysMessageVo;
import com.quan.system.entity.SysMessage;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-即时消息
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysMessageService extends MyBaseService<SysMessage> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysMessageVo> queryList(PageUtils pageUtils, SysMessageVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysMessageVo vo);
}

