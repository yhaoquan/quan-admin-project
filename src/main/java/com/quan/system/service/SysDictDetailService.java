package com.quan.system.service;

import com.quan.system.commons.vo.SysDictDetailVo;
import com.quan.system.entity.SysDictDetail;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.util.List;

/**
 * 系统-字典详情
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysDictDetailService extends MyBaseService<SysDictDetail> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysDictDetailVo> queryList(PageUtils pageUtils, SysDictDetailVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysDictDetailVo vo);
}

