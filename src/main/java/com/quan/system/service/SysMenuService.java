package com.quan.system.service;

import com.quan.commons.core.bean.R;
import com.quan.system.commons.vo.SysMenuVo;
import com.quan.system.entity.SysMenu;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 系统-菜单
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysMenuService extends MyBaseService<SysMenu> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysMenuVo> queryList(PageUtils pageUtils, SysMenuVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysMenuVo vo);

    /**
     * 菜单树形结构数据
     * @return
     */
    public List<SysMenuVo> listWithTree();

    /**
     * 查询所有菜单，组成为树状，并查询菜单对应的权限
     * @return
     */
    public List<SysMenuVo> listWithTreeOnPermission();

    /**
     * 根据ID获取父类路径ID
     * @param menuId
     * @return
     */
    public Long[] findMenuPath(Long menuId);
}

