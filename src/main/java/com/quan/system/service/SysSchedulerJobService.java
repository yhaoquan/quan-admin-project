package com.quan.system.service;

import com.quan.system.commons.vo.SysSchedulerJobVo;
import com.quan.system.entity.SysSchedulerJob;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * 系统-定时作业管理
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysSchedulerJobService extends MyBaseService<SysSchedulerJob> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysSchedulerJobVo> queryList(PageUtils pageUtils, SysSchedulerJobVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysSchedulerJobVo vo);


    /**
     * 添加新任务
     * @param vo
     * @return
     */
    public boolean startJob(SysSchedulerJobVo vo) throws ClassNotFoundException, SchedulerException;

    /**
     * 修改定时任务
     * @param vo
     * @return
     */
    public boolean updateJob(SysSchedulerJobVo vo) throws ClassNotFoundException, SchedulerException;

    /**
     * 暂停任务
     * @param jobName
     * @param jobGroup
     */
    public void pauseJob(Long id, String jobName, String jobGroup) throws SchedulerException;

    /**
     * 恢复任务
     * @param jobName
     * @param jobGroup
     */
    public void resumeJob(Long id, String jobName, String jobGroup) throws SchedulerException;

    /**
     * 触发任务
     * @param jobName
     * @param jobGroup
     */
    public void triggerJob(Long id, String jobName, String jobGroup) throws SchedulerException;

    /**
     * 根据任务名称删除定时任务
     * @param jobName
     */
    public void deleteJobByName(String jobName) throws SchedulerException;

    /**
     * 根据任务名称删除定时任务
     * @param jobName 任务名称
     * @param jobGroup 任务组
     */
    public void deleteJobByName(String jobName, String jobGroup) ;

    /**
     * 变更任务状态
     * @param id
     * @param jobStatus
     */
    public void changeStatus(Long id, String jobStatus) ;
}

