package com.quan.system.service;

import com.quan.system.commons.vo.SysPermissionVo;
import com.quan.system.entity.SysPermission;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 系统-菜单权限
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysPermissionService extends MyBaseService<SysPermission> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysPermissionVo> queryList(PageUtils pageUtils, SysPermissionVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysPermissionVo vo);

    /**
     * 根据菜单ID加载对应权限
     * @param menuId
     * @return
     */
    List<SysPermission> findPermissionByMenuId(Serializable menuId);

    /**
     * 根据菜单ID删除菜单权限
     * @param menuId
     * @return
     */
    public boolean deleteByMenuId(Serializable menuId);
}

