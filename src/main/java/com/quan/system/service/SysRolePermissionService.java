package com.quan.system.service;

import com.quan.system.commons.vo.SysRolePermissionVo;
import com.quan.system.entity.SysRolePermission;
import com.quan.commons.core.utils.PageUtils;
import com.quan.commons.core.biz.support.MyBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * 系统-角色与(菜单)权限关联
 *
 * @author yhaoquan
 * @email yhaoquan@163.com
 * @date 2021-01-04 22:24:24
 */
public interface SysRolePermissionService extends MyBaseService<SysRolePermission> {

    /**
     * 列表查询
     * @param vo
     * @return
     */
    public List<SysRolePermissionVo> queryList(PageUtils pageUtils, SysRolePermissionVo vo) ;

    /**
     * 分页查询
     * @param pageUtils
     * @param vo
     * @return
     */
    public PageUtils queryPage(PageUtils pageUtils, SysRolePermissionVo vo);

    public void deleteByRoleId(Serializable roleId);

    public void deleteByMenuId(Serializable menuId);

    /**
     * 保存角色与权限关联关系
     * @param permissions
     */
    public void saveRolePermission(List<SysRolePermission> permissions);

    /**
     * 根据角色ID获取对应的权限
     * @param roleId
     * @return
     */
    public List<SysRolePermission> getPermissionByRoleId(Long roleId);

    /**
     * 根据角色ID获取对应的权限
     * @param roleIds
     * @return
     */
    public List<SysRolePermission> getPermissionByRoleIds(List<Long> roleIds);
}

