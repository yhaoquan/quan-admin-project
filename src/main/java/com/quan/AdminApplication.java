package com.quan;

import com.quan.commons.core.bean.R;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@Slf4j
@EnableScheduling
@SpringBootApplication
@MapperScan("com.quan.**.mapper")
public class AdminApplication implements CommandLineRunner, DisposableBean {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("**********************************************");
        log.info("系统后台服务启动.....");
        log.info("**********************************************");
    }

    @Override
    public void destroy() throws Exception {
        log.info("**********************************************");
        log.info("系统后台服务关闭.....");
        log.info("**********************************************");
    }

    @RestController
    class CheckWebController {
        @GetMapping
        public R check() {
            return R.ok("系统后台服务启动" + new Date());
        }
    }


}
