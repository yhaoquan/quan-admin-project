package com.quan.generator.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.biz.support.MyBaseMapper;
import com.quan.generator.entity.TableEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface MySQLGeneratorMapper extends MyBaseMapper<TableEntity> {

    List<Map<String, String>> selectDatabase(@Param("params") Map<String, Object> map);

    IPage<Map<String, Object>> selectTables(IPage<Map<String, Object>> page, @Param("params") Map<String, Object> map);

    Map<String, String> selectTable(@Param("params") Map<String, Object> map);

    List<Map<String, String>> selectColumns(@Param("params") Map<String, Object> map);

}
