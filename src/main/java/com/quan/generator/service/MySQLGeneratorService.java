package com.quan.generator.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.quan.commons.core.utils.PageUtils;

import java.util.List;
import java.util.Map;


public interface MySQLGeneratorService {

    public List<Map<String, String>> selectDatabase(Map<String, Object> params);

    public IPage<Map<String, Object>> selectTables(PageUtils pageUtils, Map<String, Object> params);

    public Map<String, String> selectTable(Map<String, Object> params);

    public List<Map<String, String>> selectColumns(Map<String, Object> params);

    public byte[] generatorCode(String database, String packageName, String moduleName, List<String> tables);
}
