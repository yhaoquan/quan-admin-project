package com.quan.generator;

import lombok.Data;

import java.util.List;

@Data
public class GeneratorVo {

    /**
     * 数据库
     */
    private String database;

    /**
     * 包名 com.quan
     */
    private String packageName;

    /**
     * 模块名称 ums
     */
    private String moduleName;


    /**
     * 表名
     */
    private List<String> tables;
}
