package com.quan.commons.vo;

import lombok.Data;

import java.util.List;

/**
 * 发送的消息
 * @author yhaoquan
 */
@Data
public class MessageVo {

    /**
     * 消息ID
     */
    private Long msgId;

    /**
     * 消息类型：topic、user
     */
    private String type;

    /**
     * 消息显示方式：Notification, MessageBox, Message
     */
    private String mode;

    /**
     * 消息发送者
     */
    private String from;

    /**
     * 消息接收者
     */
    private List<String> to;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String message;

}
