package com.quan.commons.log.annotation;

import com.quan.commons.core.constant.CommonsConstant;

import java.lang.annotation.*;

/**
 * 操作日志注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    /**
     * 日志类型
     * @return
     */
    CommonsConstant.LogTypeEnums type() default CommonsConstant.LogTypeEnums.BUSINESS;

    /**
     * 描述
     * @return {String}
     */
    String value();

}
