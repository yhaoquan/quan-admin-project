package com.quan.commons.log.event;

import com.quan.system.entity.SysLog;
import org.springframework.context.ApplicationEvent;

/**
 * Spring Boot 事件
 * 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {

    public SysLogEvent(SysLog source) {
        super(source);
    }

}
