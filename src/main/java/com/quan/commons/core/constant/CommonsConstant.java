package com.quan.commons.core.constant;

/**
 * 公共常量属性
 */
public class CommonsConstant {

    /**
     * 后台管理接口路径匹配
     */
    public interface Auth {

        final String ADMIN_URL_PATTERN = "/api/admin/**";
    }

    /**
     * Redis Key 前缀
     */
    public interface RedisPrefix {
        /**
         * 单点登录token缓存key
         */
        final String SSO_TOKEN = "admin:sso:token";

        /**
         * 用户忘记密码，找回密码凭证码
         */
        final String RESET_PWD_TOKEN = "admin:reset_pwd_token";
    }

    /**
     * 业务异常
     */
    public enum BizErrorEnums {
        UNKNOW_EXCEPTION(500000, "系统未知异常"),

        VAILD_EXCEPTION(510000, "参数格式校验失败"),

        OPERATION_FAILURE(600000, "操作失败");

        private int code;
        private String msg;

        BizErrorEnums(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }

    }

    /**
     * 日志类型
     */
    public enum LogTypeEnums {
        LOGIN(1, "登录日志"),

        BUSINESS(2, "业务日志"),

        EXCEPTION(3, "异常日志"),

        UNKNOWN(4, "未知");

        private int index;

        private String name;

        private LogTypeEnums(int index, String name) {
            this.index = index;
            this.name = name;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public enum SchedulerStatus {

        RUNNING("RUNNING"),//正常
        PAUSE("PAUSE"),	//暂停
        FINISHED("FINISHED"), //完成
        FAILED("FAILED"); //失败



        private String name ;

        private SchedulerStatus(String name) {
            this.name = name;
        }

        public static String getName(String name) {
            for(SchedulerStatus actionType : SchedulerStatus.values()) {
                if(actionType.getName() == name) {
                    return actionType.getName() ;
                }
            }
            return null ;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


    }


}
