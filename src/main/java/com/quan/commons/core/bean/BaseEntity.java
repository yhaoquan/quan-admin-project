package com.quan.commons.core.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class BaseEntity {

    @TableField(exist = false)
    private String keyword;

}
