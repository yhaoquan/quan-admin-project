package com.quan.commons.core.bean;


import com.quan.commons.core.constant.CommonsConstant;

import java.util.HashMap;
import java.util.Map;

public class R extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    public R() {
        put("status", true);
        put("code", 200);
        put("msg", "操作成功");
    }

    public static R error() {
        return error(CommonsConstant.BizErrorEnums.UNKNOW_EXCEPTION.getCode(), CommonsConstant.BizErrorEnums.UNKNOW_EXCEPTION.getMsg());
    }

    public static R error(String msg) {
        return error(CommonsConstant.BizErrorEnums.UNKNOW_EXCEPTION.getCode(), msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("status", false);
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R ok(int code) {
        R r = new R();
        r.put("code", code);
        return r;
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok(String msg, Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        r.put("msg", msg);
        return r;
    }

    public static R ok() {
        return new R();
    }

    public static R failure() {
        R r = new R();
        r.put("status", false);
        r.put("code", CommonsConstant.BizErrorEnums.OPERATION_FAILURE.getCode());
        r.put("msg", "failure");
        return r;
    }

    public static R failure(String msg) {
        R r = new R();
        r.put("status", false);
        r.put("code", CommonsConstant.BizErrorEnums.OPERATION_FAILURE.getCode());
        r.put("msg", msg);
        return r;
    }

    public static R failure(int code, String msg) {
        R r = new R();
        r.put("status", false);
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R failure(Map<String, Object> map) {
        R r = new R();
        r.put("status", false);
        r.put("code", CommonsConstant.BizErrorEnums.OPERATION_FAILURE.getCode());
        r.put("msg", "failure");
        r.putAll(map);
        return r;
    }

    public static R failure(int code, Map<String, Object> map) {
        R r = new R();
        r.put("status", false);
        r.put("code", code);
        r.put("msg", "failure");
        r.putAll(map);
        return r;
    }

    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
