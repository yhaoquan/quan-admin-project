package com.quan.commons.core.bean;

import lombok.Data;

@Data
public class UploadVo {

    /**
     * 指定文件存放目录位置，不指定则使用当天日期作为目录名称
     */
    private String dirPath;

    /**
     * 指定文件名称，不指定则生成随机文件名称
     */
    private String fileName;

    /**
     * 点击文件打开的链接，一般用于图片
     */
    private String href;

    /**
     * 是否生成日期目录
     */
    private boolean genDataFolder = true;

}
