package com.quan.commons.core.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 配置WebSocket SockJS
 * 实现接口来配置WebSocket请求的路径和拦截
 * 使用SockJS来访问
 * 
 * @EnableWebSocketMessageBroker
 * 注解开启使用STOMP协议来传输基于代理(message broker)的消息,这时控制器支持使用@MessageMapping,就像使用@RequestMapping一样
 * 访问URL：http://localhost:8888/portfolio
 * 
 * @author yhaoquan
 *
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketSockJSConfig implements WebSocketMessageBrokerConfigurer {
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/websocket").setAllowedOrigins("*").withSockJS();
		
	}
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		// 广播订阅(/server)
		// 点对点聊天消息代理(user)
		// 多人聊天消息代理（chat）
		registry.enableSimpleBroker("/server", "/user");
	}



}
