package com.quan.commons.core.configuration.mybatisplus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 数据填充
 */
@Component
public class MyBatisPlusMetaObjectHandler implements MetaObjectHandler {

    /**
     * 使用MP实现添加操作，执行该方法
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {

        // 插入数据时填充ctime 和 utime 数据
        this.setFieldValByName("ctime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("utime", LocalDateTime.now(), metaObject);
    }

    /**
     * 使用MP实现修改操作，执行该方法
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {

        // 修改数据时填充utime数据
        this.setFieldValByName("utime", LocalDateTime.now(), metaObject);
    }
}
