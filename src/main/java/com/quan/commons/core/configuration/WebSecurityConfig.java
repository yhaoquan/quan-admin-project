package com.quan.commons.core.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器注册配置
 * 
 * @author yhaoquan
 *
 */
@Configuration
public class WebSecurityConfig implements WebMvcConfigurer {

	@Autowired
	private WebSecurityAuthTokenInterceptor authTokenInterceptor;

	/**
	 * 拦截器需要通过这里添加注册才能生效
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration addInterceptor = registry.addInterceptor(authTokenInterceptor);

		// 拦截配置
		addInterceptor.addPathPatterns("/**");

		// 排除配置
		addInterceptor.excludePathPatterns("/system/sysuser/login");
		addInterceptor.excludePathPatterns("/system/sysuser/logout");
		addInterceptor.excludePathPatterns("/system/user/forgetPasswordSendMail");
		addInterceptor.excludePathPatterns("/api/**");

		addInterceptor.excludePathPatterns("/websocket/**");
		addInterceptor.excludePathPatterns("/generator/**");

		// Swagger API 排除拦截
		addInterceptor.excludePathPatterns("/swagger**");
		addInterceptor.excludePathPatterns("/webjars/**");
		addInterceptor.excludePathPatterns("/error/**");
		addInterceptor.excludePathPatterns("/swagger-resources/**");

	}

	/**
	 * 这个方法是用来配置静态资源的，比如html，js，css，等等
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}

}
