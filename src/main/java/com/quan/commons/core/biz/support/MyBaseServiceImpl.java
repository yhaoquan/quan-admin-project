package com.quan.commons.core.biz.support;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quan.commons.core.utils.PageUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 公共基础业务接口实现类
 * @param <M>
 * @param <T>
 * @author 杨浩泉
 */
@SuppressWarnings("unchecked")
public class MyBaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements MyBaseService<T> {

    @Override
    public IPage<T> selectPage(PageUtils pageUtils, T entity) {
        Page<T> page = new Page<T>(pageUtils.getPage(), pageUtils.getLimit());

        //排序
        this.handleSort(page, pageUtils);

        return super.page(page, new QueryWrapper<T>(entity));
    }

    @Override
    public IPage<T> selectPage(PageUtils pageUtils, T entity, boolean isSearchCount) {
        Page<T> page = new Page<>(pageUtils.getPage(), pageUtils.getLimit(), isSearchCount);

        //排序
        this.handleSort(page, pageUtils);

        return super.page(page, new QueryWrapper<T>(entity));
    }

    @Override
    public IPage<T> selectPage(PageUtils pageUtils, QueryWrapper<T> queryWrapper) {
        Page<T> page = new Page<>(pageUtils.getPage(), pageUtils.getLimit());

        //排序
        this.handleSort(page, pageUtils);

        return super.page(page, queryWrapper);
    }

    @Override
    public IPage<T> selectPage(PageUtils pageUtils, QueryWrapper<T> queryWrapper, boolean isSearchCount) {
        Page<T> page = new Page<>(pageUtils.getPage(), pageUtils.getLimit(), isSearchCount);

        //排序
        this.handleSort(page, pageUtils);

        return super.page(page, queryWrapper);
    }

    @Override
    public List<T> list(PageUtils pageUtils, QueryWrapper<T> queryWrapper) {
        // 排序
        if (null != pageUtils && StringUtils.isNotBlank(pageUtils.getSortColumns())) {
            // 排序字段“,”逗号分割
            String[] sortColumns = pageUtils.getSortColumns().split(",");

            if (pageUtils.getSortType().equalsIgnoreCase("DESC")) {
                queryWrapper.orderByDesc(sortColumns);
            } else {
                queryWrapper.orderByAsc(sortColumns);
            }
        }

        return super.list(queryWrapper);
    }

    @Override
    public List<T> list(PageUtils pageUtils, T entity) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(entity);
        queryWrapper.setEntity(entity);
        // 排序
        if (null != pageUtils && StringUtils.isNotBlank(pageUtils.getSortColumns())) {
            // 排序字段“,”逗号分割
            String[] sortColumns = pageUtils.getSortColumns().split(",");

            if (pageUtils.getSortType().equalsIgnoreCase("DESC")) {
                queryWrapper.orderByDesc(sortColumns);
            } else {
                queryWrapper.orderByAsc(sortColumns);
            }
        }

        return super.list(queryWrapper);
    }

    @Override
    public boolean hasExist(String column, Object val, Class<T> entity) {
        return this.hasExist(column, val, entity, null);
    }

    @Override
    public boolean hasExist(String column, Object val, Class<T> entity, Map<String, Object> queryMaps) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<T>();
        queryWrapper.eq(column, val);

        if (null != queryMaps) {
            for (String key : queryMaps.keySet()) {
                queryWrapper.eq(key, queryMaps.get(key));
            }
        }

        int count = super.count(queryWrapper);
        if (count == 0) {
            return false;    // 不存在
        } else {
            return true;    // 存在
        }
    }

    @Override
    public T hasExist(String column, Object val, Map<String, Object> queryMaps, String... selectColumns) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<T>();
        queryWrapper.eq(column, val);
        if (null != selectColumns && selectColumns.length > 0) {
            queryWrapper.select(selectColumns);
        }

        if (null != queryMaps) {
            for (String key : queryMaps.keySet()) {
                queryWrapper.eq(key, queryMaps.get(key));
            }
        }

        return super.getOne(queryWrapper);
    }

    /**
     * 排序
     * @param page      分页对象
     * @param pageUtils 分页参数
     */
    @Override
    public void handleSort(Page page, PageUtils pageUtils) {
        // 排序
        if (StringUtils.isNotBlank(pageUtils.getSortColumns())) {
            // 排序字段“,”逗号分割
            String[] sortColumns = pageUtils.getSortColumns().split(",");

            if (pageUtils.getSortType().equalsIgnoreCase("DESC")) {
                page.setOrders(OrderItem.descs(sortColumns));
            } else {
                page.setOrders(OrderItem.ascs(sortColumns));
            }
        }
    }

    /**
     * 排序
     * @param queryWrapper
     * @param pageUtils
     */
    @Override
    public void handleSort(QueryWrapper<T> queryWrapper, PageUtils pageUtils) {
        // 排序
        if (StringUtils.isNotBlank(pageUtils.getSortColumns())) {
            // 排序字段“,”逗号分割
            String[] sortColumns = pageUtils.getSortColumns().split(",");

            if (pageUtils.getSortType().equalsIgnoreCase("DESC")) {
                queryWrapper.orderByDesc(sortColumns);
            } else {
                queryWrapper.orderByAsc(sortColumns);
            }
        }
    }

}
