package com.quan.commons.core.biz.support;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 公共基础Mapper
 * @param <T>
 * @author 杨浩泉
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {

}
