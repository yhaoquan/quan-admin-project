package com.quan.commons.core.biz.support;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.quan.commons.core.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 公共基础业务接口
 * @param <T>
 * @author 杨浩泉
 */
public interface MyBaseService<T> extends IService<T> {

    /**
     * 分页查询
     * @param entity    实体对象
     * @param pageUtils 分页对象
     * @return
     */
    public IPage<T> selectPage(PageUtils pageUtils, T entity);

    /**
     * 分页查询，不统计总记录数和总页数
     * @param entity        实体对象
     * @param pageUtils     分页对象
     * @param isSearchCount true 不统计总记录数
     * @return
     */
    public IPage<T> selectPage(PageUtils pageUtils, T entity, boolean isSearchCount);

    /**
     * 分页查询
     * @param queryWrapper 条件构造器
     * @param pageUtils    分页对象
     * @return
     */
    public IPage<T> selectPage(PageUtils pageUtils, QueryWrapper<T> queryWrapper);

    /**
     * 分页查询
     * @param queryWrapper  条件构造器
     * @param pageUtils     分页对象
     * @param isSearchCount 是否查询统计记录数
     * @return
     */
    public IPage<T> selectPage(PageUtils pageUtils, QueryWrapper<T> queryWrapper, boolean isSearchCount);


    /**
     * 查询列表
     * @param pageUtils
     * @param queryWrapper
     * @return
     */
    public List<T> list(PageUtils pageUtils, QueryWrapper<T> queryWrapper);

    /**
     * 查询列表
     * @param pageUtils 排序
     * @param entity    查询对象实体属性
     * @return
     */
    public List<T> list(PageUtils pageUtils, T entity);

    /**
     * 判断该字段值是否存在
     * @param column 表列名
     * @param val    值
     * @param entity 对应实体
     * @return
     */
    public boolean hasExist(String column, Object val, Class<T> entity);

    /**
     * @param column    表列名
     * @param val       值
     * @param entity    对应实体
     * @param queryMaps 条件为Map(key: 表列名, value: 值)
     * @return
     */
    public boolean hasExist(String column, Object val, Class<T> entity, Map<String, Object> queryMaps);

    /**
     * 判断该字段值是否存在
     * @param column        表列名
     * @param val           值
     * @param queryMaps     条件为Map(key: 表列名, value: 值)
     * @param selectColumns 返回的列
     * @return 实体对象
     */
    public T hasExist(String column, Object val, Map<String, Object> queryMaps, String... selectColumns);


    /**
     * 排序
     * @param page      分页对象
     * @param pageUtils 分页参数
     */
    public void handleSort(Page page, PageUtils pageUtils);

    /**
     * 排序
     * @param queryWrapper
     * @param pageUtils
     */
    public void handleSort(QueryWrapper<T> queryWrapper, PageUtils pageUtils);
}
