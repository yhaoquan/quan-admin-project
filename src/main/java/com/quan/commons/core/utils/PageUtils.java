package com.quan.commons.core.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;


/**
 * 分页工具类
 */
@Data
public class PageUtils implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 总记录数
     */
    private long total = 0L;

    /**
     * 当前页数
     */
    private int page = 1;

    /**
     * 每页记录数
     */
    private int limit = 10;

    /**
     * 总页数
     */
    private long totalPage = 1;

    /**
     * 当前页所有参数
     */
    private String queryString;

    /**
     * 排序的表字段名称，允许多个字段，用逗号","分割
     * "price", "sort", "ctime"
     */
    private String sortColumns;

    /**
     * 排序类型（DESC：降序，ASC：降序）
     */
    private String sortType = "DESC";

    private List<?> records;

    public PageUtils() {
    }

    /**
     * 分页
     */
    public PageUtils(IPage<?> page) {
        this.records = page.getRecords();
        this.total = (int) page.getTotal();
        this.limit = (int) page.getSize();
        this.page = (int) page.getCurrent();
        this.totalPage = (int) page.getPages();
    }

    /**
     * 如果请求为POST并且设置为@RequestBody将无法获取Paramster参数
     * @param request
     */
    public PageUtils(HttpServletRequest request) {
        init(request);
    }

    public PageUtils(long total) {
        this.total = total;
    }

    public PageUtils(long total, int page, int limit) {
        this.total = total;
        this.page = page;
        this.limit = limit;
    }

    public PageUtils(long total, int page, int limit, String sortColumns, String sortType) {
        this.total = total;
        this.page = page;
        this.limit = limit;
        this.sortColumns = sortColumns;
        this.sortType = sortType;
    }

    public PageUtils(int page, int limit) {
        super();
        this.page = page;
        this.limit = limit;
    }


    private void init(HttpServletRequest request) {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String queryString = request.getQueryString();
        String sortColumns = request.getParameter("sortColumns");
        String sortType = request.getParameter("sortType");

        if (!StringUtils.isEmpty(page)) {
            setPage(Integer.parseInt(page));
        }
        if (!StringUtils.isEmpty(limit)) {
            setLimit(Integer.parseInt(limit));
        }
        if (!StringUtils.isEmpty(queryString)) {
            setQueryString(queryString);
        }
        if (!StringUtils.isEmpty(sortColumns)) {
            setSortColumns(sortColumns);
        }
        if (!StringUtils.isEmpty(sortType)) {
            setSortType(sortType);
        }
    }

    public long getTotalPage() {
        if (totalPage == 1) {
            return this.totalPage = (total % limit) == 0 ? (total / limit) : ((total / limit) + 1);
        } else {
            return this.totalPage;
        }
    }

    public static void main(String[] args) {
        PageUtils p = new PageUtils();
        p.setTotal(50);
        System.out.println("总记录数：" + p.getTotal());
        System.out.println("当前页：" + p.getPage());
        System.out.println("每页记录数：" + p.getLimit());
        System.out.println("总页数：" + p.getTotalPage());
    }
}
