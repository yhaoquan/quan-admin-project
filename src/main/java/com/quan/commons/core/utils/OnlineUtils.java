package com.quan.commons.core.utils;

import com.quan.system.commons.vo.SysUserVo;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class OnlineUtils {

    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static AtomicInteger onlineNum = new AtomicInteger();

    //concurrent包的线程安全Set，用来存放每个客户端对应的WebSocketServer对象。
    private static ConcurrentHashMap<Long, SysUserVo> onlineUserPools = new ConcurrentHashMap<>();

    public static void addOnlineCount(){
        onlineNum.incrementAndGet();
    }

    public static void subOnlineCount() {
        onlineNum.decrementAndGet();
    }

    public static AtomicInteger getOnlineNumber() {
        return onlineNum;
    }

    /**
     * 用户上线
     * @param userVo
     */
    public static void setOnlineUserPools(SysUserVo userVo) {

        if(null == getOnlineUser(userVo.getId())) {
            // 添加用户到池中
            onlineUserPools.put(userVo.getId(), userVo);
            // 添加人数
            addOnlineCount();

            log.info("【{}】上线，当前人数：{}", userVo.getUsername(), OnlineUtils.getOnlineNumber());
        }
    }

    /**
     * 用户下线
     * @param userId
     */
    public static void setUnlineUserPools(Long userId) {
        final SysUserVo onlineUser = getOnlineUser(userId);
        if(null != onlineUser) {
            // 添加用户到池中
            onlineUserPools.remove(userId);
            // 减人数
            subOnlineCount();

            log.info("【{}】下线，当前人数：{}", onlineUser.getUsername(), OnlineUtils.getOnlineNumber());
        }
    }

    public static SysUserVo getOnlineUser(Long userId) {
        final ConcurrentHashMap<Long, SysUserVo> onlineUserPools = getOnlineUserPools();
        return onlineUserPools.get(userId);
    }


    public static ConcurrentHashMap<Long, SysUserVo> getOnlineUserPools() {
        return onlineUserPools;
    }
}
