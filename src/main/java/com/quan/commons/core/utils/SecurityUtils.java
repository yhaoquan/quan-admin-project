package com.quan.commons.core.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;


public class SecurityUtils {

    /**
     * Base64 编码
     * @param str
     * @return
     */
    public static String encodeBase64(String str) {
        return Base64.encodeBase64String(str.getBytes());
    }

    /**
     * Base64 解码
     * @param str
     * @return
     */
    public static String decodeBase64(String str) {
        return StringUtils.newStringUtf8(Base64.decodeBase64(str));
    }

    /**
     * MD5加密
     * @param encryptStr 加密字符串
     * @return
     */
    public static String md5(String encryptStr) {
        return DigestUtils.md5Hex(encryptStr);
    }

    /**
     * MD5加密
     * @param encryptStr 加密字符串
     * @param salt       盐
     * @return
     */
    public static String md5(String encryptStr, String salt) {
        return DigestUtils.md5Hex(encryptStr + salt);
    }

    /**
     * SHA-1 加密
     * @param encryptStr 加密字符串
     * @return
     */
    public static String sha1(String encryptStr) {
        return DigestUtils.sha1Hex(encryptStr);
    }

    /**
     * SHA-1 加密
     * @param encryptStr 加密字符串
     * @param salt       盐
     * @return
     */
    public static String sha1(String encryptStr, String salt) {
        return DigestUtils.sha1Hex(encryptStr + salt);
    }

    /**
     * SHA-256 加密
     * @param encryptStr 加密字符串
     * @return
     */
    public static String sha256(String encryptStr) {
        return DigestUtils.sha256Hex(encryptStr);
    }

    /**
     * SHA-256 加密
     * @param encryptStr 加密字符串
     * @param salt       盐
     * @return
     */
    public static String sha256(String encryptStr, String salt) {
        return DigestUtils.sha256Hex(encryptStr + salt);
    }

    /**
     * SHA-384 加密
     * @param encryptStr 加密字符串
     * @return
     */
    public static String sha384(String encryptStr) {
        return DigestUtils.sha384Hex(encryptStr);
    }

    /**
     * SHA-384 加密
     * @param encryptStr 加密字符串
     * @param salt       盐
     * @return
     */
    public static String sha384(String encryptStr, String salt) {
        return DigestUtils.sha384Hex(encryptStr + salt);
    }

    /**
     * SHA-512 加密
     * @param encryptStr 加密字符串
     * @return
     */
    public static String sha512(String encryptStr) {
        return DigestUtils.sha512Hex(encryptStr);
    }

    /**
     * SHA-512 加密
     * @param encryptStr 加密字符串
     * @param salt       盐
     * @return
     */
    public static String sha512(String encryptStr, String salt) {
        return DigestUtils.sha512Hex(encryptStr + salt);
    }


    public static void main(String[] args) throws Exception {
        String encryptStr = "Hello";
        String salt = "123";

        System.out.println("MD5：" + md5(encryptStr));
        System.out.println("MD5+salt：" + md5(encryptStr, salt));
        System.out.println("-----------------------------------------");
        System.out.println("SHA-1：" + sha1(encryptStr));
        System.out.println("SHA-1+salt：" + sha1(encryptStr, salt));
        System.out.println("-----------------------------------------");
        System.out.println("SHA-256：" + sha256(encryptStr));
        System.out.println("SHA-256+salt：" + sha256(encryptStr, salt));
        System.out.println("-----------------------------------------");
        System.out.println("SHA-384：" + sha384(encryptStr));
        System.out.println("SHA-384+salt：" + sha384(encryptStr, salt));
        System.out.println("-----------------------------------------");
        System.out.println("SHA-512：" + sha512(encryptStr));
        System.out.println("SHA-512+salt：" + sha512(encryptStr, salt));
        System.out.println("-----------------------------------------");
        String encodeBase64 = encodeBase64(encryptStr);
        System.out.println("BASE64-encode：" + encodeBase64(encryptStr));
        System.out.println("BASE64-decode：" + decodeBase64(encodeBase64));
        System.out.println("-----------------------------------------");


    }
}
