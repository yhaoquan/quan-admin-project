package com.quan.commons.core.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public final static String YYYY_MM = "yyyy-MM";
    public final static String YYYY_MM_DD = "yyyy-MM-dd";
    public final static String YYYYMMDD = "yyyyMMdd";
    public final static String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public final static String YYYYMMDDHHMMSSS = "yyyyMMddHHmmssSSS";

    public final static String YYYY_MM_EN = "yyyy/MM";
    public final static String YYYY_MM_DD_EN = "yyyy/MM/dd";
    public final static String YYYY_MM_DD_HH_MM_EN = "yyyy/MM/dd HH:mm";
    public final static String YYYY_MM_DD_HH_MM_SS_EN = "yyyy/MM/dd HH:mm:ss";

    public final static String YYYY_MM_ZH = "yyyy年MM月";
    public final static String YYYY_MM_DD_ZH = "yyyy年MM月dd日";
    public final static String YYYY_MM_DD_HH_MM_ZH = "yyyy年MM月dd日 HH:mm";
    public final static String YYYY_MM_DD_HH_MM_SS_ZH = "yyyy年MM月dd日 HH:mm:ss";

    public final static String HH_MM = "HH:mm";
    public final static String HH_MM_SS = "HH:mm:ss";

    public final static String MM_DD = "MM-dd";
    public final static String MM_DD_HH_MM = "MM-dd HH:mm";
    public final static String MM_DD_HH_MM_SS = "MM-dd HH:mm:ss";

    public final static String MM_DD_EN = "MM/dd";
    public final static String MM_DD_HH_MM_EN = "MM/dd HH:mm";
    public final static String MM_DD_HH_MM_SS_EN = "MM/dd HH:mm:ss";

    public final static String MM_DD_ZH = "MM月dd日";
    public final static String MM_DD_HH_MM_ZH = "MM月dd日 HH:mm";
    public final static String MM_DD_HH_MM_SS_ZH = "MM月dd日 HH:mm:ss";

    public static String format(Date date, String format) {
        return DateFormatUtils.format(date, format);
    }

    public static Date parse(String dateStr, String format) {
        try {
            return new SimpleDateFormat(format).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将日期字符串解析为日期对象
     * @param dateStr 日期字符串
     * @param pattern 格式（默认为：yyyy-MM-dd）
     * @return
     * @throws ParseException
     */
    public static Date parseDate(String dateStr, String... pattern) {
        try {
            if (null == dateStr) {
                return null;
            } else if (pattern != null && pattern.length > 0) {
                return new SimpleDateFormat(pattern.toString()).parse(dateStr);
            } else if (pattern == null || pattern.length == 0) {
                return new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(dateStr);
            } else {
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将日期对象格式化为日期字符串
     * @param date    日期对象
     * @param pattern 格式（默认为：yyyy-MM-dd）
     * @return
     */
    public static String formatDate(Date date, Object... pattern) {
        String formatDate = null;
        if (pattern != null && pattern.length > 0) {
            formatDate = DateFormatUtils.format(date, pattern[0].toString());
        } else {
            formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatMillSecDateTime(long date) {
        return formatDate(new Date(date), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime() {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd）
     */
    public static String getDate() {
        return getDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String getDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String getDateTime() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     */
    public static String getWeek() {
        return formatDate(new Date(), "E");
    }

    /**
     * 获取过去的天数
     * @param date
     * @return
     */
    public static long pastDays(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (24 * 60 * 60 * 1000);
    }

    /**
     * 获取过去的小时
     * @param date
     * @return
     */
    public static long pastHour(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (60 * 60 * 1000);
    }

    /**
     * 获取过去的分钟
     * @param date
     * @return
     */
    public static long pastMinutes(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (60 * 1000);
    }

    /**
     * 转换为时间（天,时:分:秒.毫秒）
     * @param timeMillis
     * @return
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = (timeMillis / (60 * 60 * 1000) - day * 24);
        long min = ((timeMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long sss = (timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 获取两个日期之间的天数
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
    }

    /**
     * 日期加天数,可以向前加，向后加
     * @param date 日期
     * @param day  天数
     * @return
     */
    public static Date addDay(Date date, int day) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
        return c.getTime();
    }

    /**
     * 当前时间加减分钟
     * @param minute +5 / -5
     * @return
     */
    public static long computeMinute(int minute) {
        return computeDatetime(new Date(), Calendar.MINUTE, minute);
    }

    /**
     * 当前时间加减小时
     * @param hour +5 / -5
     * @return
     */
    public static long computeHour(int hour) {
        return computeDatetime(new Date(), Calendar.HOUR, hour);
    }

    /**
     * 日期时间加分钟
     *
     * @param date 日期
     * @return 毫秒
     */
    /**
     * 日期时间加减
     * @param date 日期
     * @param type 加减类型
     * @param num  +5 / -5
     * @return
     */
    public static long computeDatetime(Date date, int type, int num) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(type, num);
        return c.getTimeInMillis();
    }

    /**
     * 获取当前日期的0点
     * @return
     */
    public static long getZeroMilliSecond() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        Date beginOfDate = cal.getTime();
        return beginOfDate.getTime();
    }

    /**
     * 获取当前日期的24点，即明天的0点的毫秒数
     * @return
     */
    public static long get24HourMilliSecond() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 24, 00, 00);
        Date endOfDate = cal.getTime();
        return endOfDate.getTime();
    }

    /**
     * 返回日期代表的毫秒
     * @param date 日期
     * @return
     */
    public static long getMillis(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.getTimeInMillis();
    }

    /**
     * 两个日期相差的天数 date1 - date2 剩余天数
     * @param date1 日期
     * @param date2 日期
     * @return
     */
    public static int betweenTwoDates(Date date1, Date date2) {
        return (int) ((getMillis(date1) - getMillis(date2)) / (24 * 3600 * 1000));
    }

    /**
     * java.util.Date --> java.time.LocalDateTime
     * @return
     */
    public static LocalDateTime UDateToLocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * java.util.Date --> java.time.LocalDate
     * @return
     */
    public static LocalDate UDateToLocalDate(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.toLocalDate();
    }

    /**
     * java.util.Date --> java.time.LocalTime
     * @return
     */
    public static LocalTime UDateToLocalTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime.toLocalTime();
    }

    /**
     * java.time.LocalDateTime --> java.util.Date
     * @return
     */
    public static Date LocalDateTimeToUdate(LocalDateTime localDateTime) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * java.time.LocalDate --> java.util.Date
     * @return
     */
    public static Date LocalDateToUdate(LocalDate localDate) {
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * java.time.LocalTime --> java.util.Date
     * @return
     */
    public static Date LocalTimeToUdate(LocalDate localDate, LocalTime localTime) {
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        ZoneId zone = ZoneId.systemDefault();
        Instant instant = localDateTime.atZone(zone).toInstant();
        return Date.from(instant);
    }

    /**
     * 当前时间减去多少小时
     * @param hours
     * @return
     */
    public static LocalDateTime minusHours(int hours) {
        return LocalDateTime.now().minusHours(hours);
    }

    /**
     * 当前时间减去多少分钟
     * @param minutes
     * @return
     */
    public static LocalDateTime minusMinutes(int minutes) {
        return LocalDateTime.now().minusMinutes(minutes);
    }

    /**
     * @param nowTime   当前时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     * @author sunran   判断当前时间在时间区间内
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        System.out.println(date.getTime().getTime());
        System.out.println(begin.getTime().getTime());

        return date.getTime().getTime() >= begin.getTime().getTime() && date.getTime().getTime() <= end.getTime().getTime();

        //if (date.equals(begin) || (date.after(begin) && date.before(end))) {
        //    return true;
        //} else {
        //    return false;
        //}
    }

    public static void main(String[] args) throws Exception {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        System.out.println(DateUtils.formatDateTime(c.getTime()));

        c.add(Calendar.MINUTE, 5);
        c.add(Calendar.SECOND, 5);

        System.out.println(DateUtils.formatDateTime(c.getTime()));

        System.out.println(computeMinute(10));
        System.out.println(formatMillSecDateTime(computeMinute(10)));

        Date parseDate = parseDate("2018-02-13 14:49:45");
        System.out.println(formatMillSecDateTime(computeDatetime(parseDate, Calendar.HOUR, 10)));

        /*
         * System.out.println(formatDateTime(addDay(new Date(), -1)));
         * System.out.println(formatDateTime(addDay(new Date(), 1)));
         *
         * int betweenTwoDates = betweenTwoDates(parseDate("2017-11-19"),
         * parseDate("2017-11-19", FORMAT_DATE)) ; System.out.println(betweenTwoDates);
         * System.out.println(DateUtils.getDate(DateUtils.FORMAT_DATETIMES_NOTHING));
         * System.out.println(DateUtils.getDate(DateUtils.FORMAT_DATETIMES_NOTHING));
         * System.out.println(DateUtils.getDate(DateUtils.FORMAT_DATETIMES_NOTHING));
         */

        // System.out.println(format(new Date(), DateStyle.YYYY_MM_DD));
        // System.out.println(parse("2018-02-13 14:49:45", DateStyle.YYYY_MM_DD));

        System.out.println("-------------------------------");
        LocalDate today = LocalDate.now();
        System.out.println(today);
        System.out.println(today.getYear());
        System.out.println(today.getMonthValue());
        System.out.println(today.getDayOfMonth());

        LocalTime time = LocalTime.now();
        LocalTime newTime = time.plusHours(2); // adding two hours

        DateTimeFormatter ofPattern = DateTimeFormatter.ofPattern("HH:mm:ss");
        System.out.println("加两小时 : " + newTime.format(ofPattern));

        System.out.println(DateUtils.UDateToLocalDate(DateUtils.parse("1988-05-11", YYYY_MM_DD)));

        String ss1 = "2004-09-15";
        double distanceOfTwoDate = DateUtils.getDistanceOfTwoDate(DateUtils.parse(ss1, YYYY_MM_DD), new Date());

        System.out.println(distanceOfTwoDate);

        System.out.println(distanceOfTwoDate / 365);
        System.out.println(MathUtil.format((distanceOfTwoDate / 365), "#.#"));

//		LocalDate date = LocalDate.now();
//		LocalDate end = date.plusDays(30);
//	    DateTimeFormatter formatters = DateTimeFormatter.ofPattern("MM-dd");
//	    String text1 = date.format(formatters);
//	    String text2 = end.format(formatters);
//	    System.out.println(text1);
//	    System.out.println(text2);

        Date zzDate = DateUtils.parse("2019-07-26", YYYY_MM_DD);
        Date curDate = DateUtils.parse(DateUtils.getDate(), YYYY_MM_DD);

        long zzMillis = DateUtils.getMillis(zzDate);
        long curMillis = DateUtils.getMillis(curDate);

        System.out.println(zzMillis);
        System.out.println(curMillis);

        LocalDateTime dateTime = LocalDateTime.now().minusHours(9);

        System.out.println(dateTime);

        Calendar cc = Calendar.getInstance();
        cc.setTime(new Date());
        //cc.add(Calendar.HOUR, -1);
        cc.add(Calendar.MINUTE, -2);

        System.out.println(formatDateTime(cc.getTime()));

        for (int i = 0; i < 10; i++) {
            final Date date = addDay(new Date(), i);
            System.out.println(format(date, "yyyy-MM-dd"));
        }


        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = ft.parse("2020-10-15");
        Date endTime = ft.parse("2020-10-15");
        Date nowTime = new Date();
        boolean effectiveDate = isEffectiveDate(nowTime, startTime, endTime);
        if (effectiveDate) {
            System.out.println("当前时间在范围内");
        } else {
            System.out.println("当前时间在不在范围内");
        }

    }

}
