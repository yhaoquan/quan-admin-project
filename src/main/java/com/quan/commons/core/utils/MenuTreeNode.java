package com.quan.commons.core.utils;

import java.util.Date;

/**
 * 菜单树转换对象
 * @author yhaoquan
 */
public class MenuTreeNode {

    /**
     * ID
     */
    private Integer id;

    /**
     * 父菜单ID
     */
    private Integer pid;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 前端URL
     */
    private String path;

    /**
     * 请求链接
     */
    private String url;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 图标
     */
    private String icon;

    /**
     * VUE页面
     */
    private String component;

    /**
     * 排序值
     */
    private Integer sort;

    /**
     * 菜单类型 （0菜单 1按钮）
     */
    private String type;

    /**
     * 状态(1:正常,2:禁用)
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 最后更新时间
     */
    private Date utime;

    public MenuTreeNode() {
    }

    public MenuTreeNode(Integer id, Integer pid, String name) {
        super();
        this.id = id;
        this.pid = pid;
        this.name = name;
    }

    public MenuTreeNode(Integer id, Integer pid, String name, String path, String url, String method, String icon, String component, Integer sort, String type, Boolean status, Date ctime, Date utime) {
        super();
        this.id = id;
        this.pid = pid;
        this.name = name;
        this.path = path;
        this.url = url;
        this.method = method;
        this.icon = icon;
        this.component = component;
        this.sort = sort;
        this.type = type;
        this.status = status;
        this.ctime = ctime;
        this.utime = utime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Date getUtime() {
        return utime;
    }

    public void setUtime(Date utime) {
        this.utime = utime;
    }

    @Override
    public String toString() {
        return "MenuTreeNode [id=" + id + ", pid=" + pid + ", name=" + name + ", path=" + path + ", url=" + url + ", method=" + method + ", icon=" + icon + ", component=" + component + ", sort=" + sort + ", type=" + type + ", status=" + status + ", ctime=" + ctime + ", utime=" + utime + "]";
    }

}
