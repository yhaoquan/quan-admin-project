package com.quan.commons.core.utils;

import cn.hutool.core.util.RandomUtil;

/**
 * 密码工具类
 * @类名 PasswordUtils
 * @日期 2016年12月16日 上午11:20:26
 * @作者 yhaoquan
 * @版权 (c) All Rights Reserved, 2016.
 */
public class PasswordUtils {

    /**
     * 密码加密<br>
     * 加密方式SHA256(密码+盐)
     * @param password 密码
     * @param salt     加密盐
     * @return
     */
    public static String encodePassword(String password, String salt) {
        return SecurityUtils.sha256(password, salt);
    }

    /**
     * 密码验证
     * @param encPassword 加密密码
     * @param password    文明密码
     * @param salt        加密盐
     * @return
     */
    public static boolean isPasswordValid(String encPassword, String password, String salt) {
        if (SecurityUtils.sha256(password, salt).equals(encPassword)) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        String salt = RandomUitl.uuid();
        String password = "orange";
        String encodePassword = encodePassword(password, salt);
        System.out.println(salt);
        System.out.println(encodePassword);
        System.out.println(isPasswordValid(encodePassword, password, salt));

    }

}
