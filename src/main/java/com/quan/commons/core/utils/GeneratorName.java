package com.quan.commons.core.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class GeneratorName {

    /**
     * 生成文件名
     * 1：文件名的前缀_yyyyMMddHHmmssSSS_随机数
     * 2：yyyyMMddHHmmssSSS_随机数
     * @param originalFilename 文件原始名称
     * @param prefixName       新文件名的前缀
     * @return
     */
    public static String fileName(String originalFilename, String prefixName) {
        String extension = FilenameUtils.getExtension(originalFilename);
        if (StringUtils.isNotBlank(prefixName)) {
            return prefixName + "_" + DateUtils.getDate(DateUtils.YYYYMMDDHHMMSSS) + "." + extension;
        } else {
            return DateUtils.getDate(DateUtils.YYYYMMDDHHMMSSS) + "." + extension;
        }
    }

}
