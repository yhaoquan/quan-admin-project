package com.quan.commons.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 系统常量配置
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "system")
public class SystemValueProperties {

    /**
     * 系统名称
     */
    private String name;

    /**
     * 邮件配置
     */
    private Mail mail;

    /**
     * 阿里云相关配置
     */
    private Aliyun aliyun;

    /**
     * JwtToken秘钥
     */
    private Jwt jwt;

    /**
     * 地图Key
     */
    private Amap amap;

    /**
     * 微信
     */
    private Wechat wechat;

    /**
     * 文件上传路径
     */
    private String upload;

    /**
     * Redisson连接地址
     */
    private String redissonAddr;

    /**
     * 申请创建活动收取短信的手机号码
     */
    private String applyActivityToPhone;

    @Data
    public static class Mail {
        private String from;
        private String personal;
        private String bbc;
    }

    @Data
    public static class Wechat {
        private String h5HomeUrl;
        private String h5LoginUrl;
        private String oauthUrl;
        private String oauthCallbackUrl;
        private String payNotifyUrl;
        private String scanpayNotifyUrl;

        private String oauthTenantSalesUrl;
        private String oauthTenantSalesCallbackUrl;
    }

    @Data
    public static class Aliyun {
        private Sms sms;

        @Data
        public static class Sms {
            private String keyId;
            private String keySecret;
            private List<Signs> signs;

            @Data
            public static class Signs {
                private String name;
                private String templateCode;
            }
        }

        private Oss oss;

        @Data
        public static class Oss {
            private String accessKeyId;
            private String secretAccessKey;
            private String endPoint;
            private String bucketName;
        }
    }

    @Data
    public static class Jwt {
        /**
         * token 标识
         */
        private String header;
        /**
         * 秘钥
         */
        private String secret;
        /**
         * 过期时间：秒
         */
        private Long timeout;

    }

    @Data
    public static class Amap {
        private String key;
    }

}
