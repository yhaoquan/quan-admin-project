package com.quan.commons.tools.mail.vo;

import lombok.Data;

import java.io.File;
import java.io.Serializable;
import java.util.Map;

@Data
public class MailVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发送人
     */
    private String from;

    /**
     * 个性签名
     */
    private String personal;

    /**
     * 发送给谁
     */
    private String to;

    /**
     * 抄送给谁
     */
    private String cc;

    /**
     * 暗送给谁
     */
    private String bcc;

    /**
     * 邮件标题
     */
    private String subject;

    /**
     * 邮件内容
     */
    private String content;

    /**
     * HTML邮件模板路径，模板统一存放在/resources/templates/mail
     */
    private String templateLocation;

    /**
     * HTML邮件模板数据
     */
    private Map<String, Object> templateData;

    /**
     * 附件
     */
    private File file;

}
