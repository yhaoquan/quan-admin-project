package com.quan.commons.tools.mail;

import com.quan.commons.core.properties.SystemValueProperties;
import com.quan.commons.tools.mail.vo.MailVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;

@Slf4j
@Component
public class MailTools {

    @Autowired
    private SystemValueProperties properties;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    public void send(String to, String subject, String content) {
        send(null, null, to, null, null, subject, content, null, null, null);
    }

    public void send(String to, String cc, String subject, String content) {
        send(null, null, to, cc, null, subject, content, null, null, null);
    }

    public void send(String to, String subject, String content, File file) {
        send(null, null, to, null, null, subject, content, null, null, file);
    }

    public void send(String from, String personal, String to, String cc, String subject, String content, File file) {
        send(from, personal, to, cc, null, subject, content, null, null, file);
    }

    public void send(String from, String personal, String to, String cc, String bcc, String subject, String content, File file) {
        send(from, personal, to, cc, bcc, subject, content, null, null, file);
    }


    public void send(String to, String subject, String templateLocation, Map<String, Object> data) {
        send(null, null, to, null, null, subject, null, templateLocation, data, null);
    }

    public void send(String to, String cc, String subject, String templateLocation, Map<String, Object> data) {
        send(null, null, to, cc, null, subject, null, templateLocation, data, null);
    }

    public void send(String to, String subject, String templateLocation, Map<String, Object> data, File file) {
        send(null, null, to, null, null, subject, null, templateLocation, data, file);
    }

    public void send(String to, String cc, String subject, String templateLocation, Map<String, Object> data, File file) {
        send(null, null, to, cc, null, subject, null, templateLocation, data, file);
    }

    public void send(String to, String cc, String subject, String bcc, String templateLocation, Map<String, Object> data, File file) {
        send(null, null, to, cc, bcc, subject, null, templateLocation, data, file);
    }

    public void send(String from, String personal, String to, String cc, String subject, String templateLocation, Map<String, Object> data, File file) {
        send(from, personal, to, cc, null, subject, null, templateLocation, data, file);
    }

    public void send(String from, String personal, String to, String cc, String bcc, String subject, String templateLocation, Map<String, Object> data, File file) {
        send(from, personal, to, cc, bcc, subject, null, templateLocation, data, file);
    }

    public void send(MailVo bean) {
        send(bean.getFrom(), bean.getPersonal(), bean.getTo(), bean.getCc(), bean.getBcc(), bean.getSubject(), bean.getContent(), bean.getTemplateLocation(), bean.getTemplateData(), bean.getFile());
    }

    public void send(String from, String personal, String to, String cc, String bcc, String subject, String plainText, String templateLocation, Map<String, Object> templateModel, File file) {

        if (StringUtils.isBlank(this.properties.getMail().getFrom())) {
            throw new RuntimeException("请在配置文件中配置发送者邮件地址：mail.form");
        }
        if (StringUtils.isBlank(this.properties.getMail().getPersonal())) {
            throw new RuntimeException("请在配置文件中配置发送者中文名：mail.personal");
        }

        //设置暗送收件人
        if (StringUtils.isBlank(bcc)) {
            if (StringUtils.isNotBlank(this.properties.getMail().getBbc())) {
                bcc = this.properties.getMail().getBbc();
            }
        }

        //邮件内容
        String content = null;

        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);

            String f = StringUtils.isNotBlank(from) ? from : this.properties.getMail().getFrom();
            String p = StringUtils.isNotBlank(personal) ? personal : this.properties.getMail().getPersonal();

            message.setFrom(f, p);

            message.setSubject(subject);

            //发送至
            if (StringUtils.isNotBlank(to)) {
                String[] split = StringUtils.split(to, ",");
                message.setTo(split);
            }
            //抄送
            if (StringUtils.isNotBlank(cc)) {
                String[] split = StringUtils.split(cc, ",");
                message.setCc(split);
            }
            //暗送
            if (StringUtils.isNotBlank(bcc)) {
                String[] split = StringUtils.split(bcc, ",");
                message.setBcc(split);
            }

            //简单文本
            content = plainText;

            //HTML邮件
            if (StringUtils.isNotBlank(templateLocation)) {
                Context context = new Context();
                if (null != templateModel && !templateModel.isEmpty()) {
                    for (String key : templateModel.keySet()) {
                        context.setVariable(key, templateModel.get(key));
                    }
                }
                content = this.templateEngine.process(templateLocation, context);

            }
            message.setText(content, true);

            if (null != file) {
                //附件
                message.addAttachment(file.getName(), file);
            }

            //发送邮件
            this.mailSender.send(mimeMessage);

            log.info("==>邮件发送成功********************************");
            log.info(">>>>from：{}", f);
            log.info(">>>>to：{}", to);
            log.info(">>>>cc：{}", StringUtils.isBlank(cc) ? "无" : cc);
            log.info(">>>>bcc：{}", StringUtils.isBlank(bcc) ? "无" : bcc);
            log.info(">>>>主题：{}", subject);
            //log.info(">>>>内容：{}", content);
        } catch (Exception e) {
            log.error("==>邮件发送失败<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            log.error(">>>>error：{}", e.getMessage());
            log.error(">>>>to：{}", to);
            log.error(">>>>cc：{}", StringUtils.isBlank(cc) ? "无" : cc);
            log.error(">>>>bcc：{}", StringUtils.isBlank(bcc) ? "无" : bcc);
            log.error(">>>>主题：{}", subject);
            //log.error(">>>>内容：{}", content);
        }
    }

}
