package com.quan.junit.test.faker;

import com.github.javafaker.Faker;
import com.quan.commons.core.utils.DateUtils;
import com.quan.commons.core.utils.Pinyin4jHelper;
import com.quan.system.commons.vo.SysUserVo;
import com.quan.system.entity.SysUser;
import com.quan.junit.test.ApplicationTests;
import com.quan.system.service.SysUserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;

public class SystemUserTest extends ApplicationTests {

	@Autowired
	private SysUserService service;

	@Test
	public void forge() {
		Faker faker = new Faker(Locale.CHINA);

		for(int i=0; i<100; i++) {
			SysUserVo u = new SysUserVo();
			u.setRealname(faker.name().fullName());
			u.setUsername(Pinyin4jHelper.getPinYin(faker.name().firstName()));
			u.setBirthday(DateUtils.UDateToLocalDate(faker.date().birthday()));
			u.setAvatar(faker.internet().avatar());
			u.setEmail(faker.internet().emailAddress(faker.app().name()));
			u.setMobile(faker.phoneNumber().cellPhone());
			u.setSex(faker.number().numberBetween(0, 1) == 1 ? "男" : "女");
			u.setStatus(1);
			u.setPassword("123456");

			System.out.println(u);

			this.service.saveUser(u);
		}


	}

	public static void main(String[] args) {
		Faker faker = new Faker(Locale.CHINA);

		for(int i=0; i<100; i++) {
			SysUser u = new SysUser();
			u.setRealname(faker.name().fullName());
			u.setUsername(faker.name().lastName());
			u.setBirthday(DateUtils.UDateToLocalDate(faker.date().birthday()));
			u.setAvatar(faker.internet().avatar());
			u.setEmail(faker.internet().emailAddress(faker.app().name()));
			u.setMobile(faker.phoneNumber().cellPhone());
			u.setSex(faker.number().numberBetween(0, 1) == 1 ? "男" : "女");
			u.setStatus(1);
			u.setPassword("123456");

			System.out.println(u);
		}

		//System.out.println("======================");
		//System.out.println(faker.name().prefix());
		//System.out.println(faker.name().suffix());
		//System.out.println(faker.name().bloodGroup());
		//System.out.println(faker.name().firstName());
		//System.out.println(faker.name().lastName());
		//System.out.println(faker.name().nameWithMiddle());
		//System.out.println(faker.name().title());
		//System.out.println(faker.name().username());
		//System.out.println(faker.app().name());
		//System.out.println(faker.app().author());
		//System.out.println(faker.internet().avatar());
		//System.out.println(faker.internet().emailAddress());
		//System.out.println(faker.internet().userAgentAny());
		//System.out.println(faker.code().asin());
		//System.out.println(faker.beer().name());
		//System.out.println(faker.aquaTeenHungerForce().character());
		//System.out.println(faker.number().digit());
		//System.out.println(faker.number().digits(3));
		//System.out.println(faker.number().randomNumber());
		//System.out.println(faker.number().numberBetween(0, 1));
		//System.out.println(faker.name().username());
		//System.out.println(faker.funnyName().name());
	}

}
