FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=./target/quan-admin-project.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]


#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","-Dspring.profiles.active=docker","/app.jar"]


#构建镜像
#docker build -t quan-luck-project . --build-arg JAR_FILE=quan-luck-project.jar

#运行镜像 
#docker run -p 8600:8600 quan-luck-project

##################################################################


#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#ADD ./target/springboot-docker-0.0.1-SNAPSHOT.jar app.jar
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","./target/app.jar"]


#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#ARG JAR_FILE
#ADD ${JAR_FILE} app.jar
#ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]


