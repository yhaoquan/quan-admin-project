/*
 Navicat Premium Data Transfer

 Source Server         : MySQL8
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : quan-admin-project

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 06/02/2021 15:48:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('clusteredScheduler', 'trigger-测试定时任务', 'DEFAULT', '0/5 * * * * ? *', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FIRED_TIME` bigint(0) NOT NULL,
  `SCHED_TIME` bigint(0) NOT NULL,
  `PRIORITY` int(0) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('clusteredScheduler', '测试定时任务', 'DEFAULT', '111111', 'com.quan.system.commons.job.PushMsgToClientJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787000737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F40000000000010770800000010000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('clusteredScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('clusteredScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(0) NOT NULL,
  `CHECKIN_INTERVAL` bigint(0) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('clusteredScheduler', 'LAPTOP-8KRUIOQH1612539941973', 1612597718251, 10000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REPEAT_COUNT` bigint(0) NOT NULL,
  `REPEAT_INTERVAL` bigint(0) NOT NULL,
  `TIMES_TRIGGERED` bigint(0) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(0) NULL DEFAULT NULL,
  `INT_PROP_2` int(0) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(0) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(0) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(0) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(0) NULL DEFAULT NULL,
  `PRIORITY` int(0) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `START_TIME` bigint(0) NOT NULL,
  `END_TIME` bigint(0) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(0) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('clusteredScheduler', 'trigger-测试定时任务', 'DEFAULT', '测试定时任务', 'DEFAULT', NULL, 1612536940000, 1612536935000, 5, 'PAUSED', 'CRON', 1612536462000, 0, NULL, 0, '');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '唯一编码',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态(0禁用:,1:正常)',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1325982968048529410, '床型', 'CX', 1, NULL, '2020-11-10 10:05:50', '2020-11-10 10:05:50');
INSERT INTO `sys_dict` VALUES (1325983327353581569, '餐饮', 'CY', 1, NULL, '2020-11-10 10:07:15', '2020-11-10 10:36:58');
INSERT INTO `sys_dict` VALUES (1325990867273527297, '网络', 'WL', 1, NULL, '2020-11-10 10:37:13', '2020-11-10 10:37:13');
INSERT INTO `sys_dict` VALUES (1325990925045870594, '吸烟政策', 'XYZC', 1, NULL, '2020-11-10 10:37:27', '2020-11-10 10:38:53');
INSERT INTO `sys_dict` VALUES (1326162414768545794, '客房特色', 'FEATURE', 1, NULL, '2020-11-10 21:58:53', '2020-11-10 21:58:53');
INSERT INTO `sys_dict` VALUES (1326163737501032450, '基础设施与服务', 'ENV', 1, NULL, '2020-11-10 22:04:09', '2020-11-10 22:04:09');
INSERT INTO `sys_dict` VALUES (1326167143477260290, '酒店类型', 'HOTEL_TYPE', 1, NULL, '2020-11-10 22:17:41', '2020-11-10 22:17:41');

-- ----------------------------
-- Table structure for sys_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `dict_id` bigint(0) NULL DEFAULT NULL COMMENT '字典ID',
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典标签',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典值',
  `selected` tinyint(1) NULL DEFAULT 0 COMMENT '设置默认选中(0：否，1：是)',
  `sort` int(0) NOT NULL DEFAULT 99 COMMENT '排序',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态(0禁用:,1:正常)',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-字典详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES (1, 2, '技术基层', '技术基层', 0, 9911, 1, NULL, '2019-06-26 16:07:26', '2021-01-07 12:24:40');
INSERT INTO `sys_dict_detail` VALUES (2, 3, '专科', '专科', 0, 991, 1, NULL, '2019-06-26 17:08:19', '2021-01-07 12:24:45');
INSERT INTO `sys_dict_detail` VALUES (3, 3, '本科', '本科', 1, 99, 1, NULL, '2019-06-26 17:08:38', '2020-08-09 22:53:07');
INSERT INTO `sys_dict_detail` VALUES (5, 3, '硕士', '硕士', 0, 99, 1, NULL, '2019-06-26 17:09:05', '2019-06-26 17:09:05');
INSERT INTO `sys_dict_detail` VALUES (6, 3, '博士', '博士', 0, 99, 1, NULL, '2019-06-26 17:09:23', '2019-06-26 17:09:23');
INSERT INTO `sys_dict_detail` VALUES (7, 3, '其他', '其他', 0, 99, 1, NULL, '2019-06-26 17:09:50', '2019-06-26 17:09:50');
INSERT INTO `sys_dict_detail` VALUES (10, 2, '技术中层', '技术中层', 0, 99, 1, NULL, '2019-06-26 16:07:26', '2019-06-26 16:07:26');
INSERT INTO `sys_dict_detail` VALUES (11, 2, '技术高层', '技术高层', 0, 99, 1, NULL, '2019-06-26 16:07:26', '2019-06-26 16:07:26');
INSERT INTO `sys_dict_detail` VALUES (12, 2, '管理基层', '管理基层', 0, 99, 1, NULL, '2019-06-26 16:07:26', '2019-06-26 16:07:26');
INSERT INTO `sys_dict_detail` VALUES (13, 2, '管理中层', '管理中层', 0, 99, 1, NULL, '2019-06-26 16:07:26', '2019-06-26 16:07:26');
INSERT INTO `sys_dict_detail` VALUES (14, 2, '管理高层', '管理高层', 0, 99, 1, NULL, '2019-06-26 16:07:26', '2019-06-26 16:07:26');
INSERT INTO `sys_dict_detail` VALUES (51, 13, '在职', '在职', 0, 99, 1, NULL, '2019-07-03 23:13:00', '2019-07-03 23:13:00');
INSERT INTO `sys_dict_detail` VALUES (52, 13, '离职', '离职', 0, 99, 1, NULL, '2019-07-03 23:13:06', '2019-07-03 23:13:06');
INSERT INTO `sys_dict_detail` VALUES (53, 13, '停薪留职', '停薪留职', 0, 99, 1, NULL, '2019-07-03 23:13:13', '2019-07-03 23:13:13');
INSERT INTO `sys_dict_detail` VALUES (1325983041310437378, 1325982968048529410, '大床', '大床', 1, 9911, 1, NULL, '2020-11-10 10:06:07', '2021-01-07 12:26:06');
INSERT INTO `sys_dict_detail` VALUES (1325983123036450818, 1325982968048529410, '双床', '双床', 0, 991, 1, NULL, '2020-11-10 10:06:27', '2021-01-07 12:24:34');
INSERT INTO `sys_dict_detail` VALUES (1325983209845960706, 1325982968048529410, '多床', '多床', 0, 99, 1, NULL, '2020-11-10 10:06:47', '2020-11-10 10:06:47');
INSERT INTO `sys_dict_detail` VALUES (1325983399206203393, 1325983327353581569, '含早餐', '含早餐', 0, 99, 1, NULL, '2020-11-10 10:07:33', '2020-11-10 10:07:33');
INSERT INTO `sys_dict_detail` VALUES (1325983422916603905, 1325983327353581569, '不含早餐', '不含早餐', 1, 99, 1, NULL, '2020-11-10 10:07:38', '2020-12-08 18:17:55');
INSERT INTO `sys_dict_detail` VALUES (1325991017731600386, 1325990867273527297, '免费WiFi', '免费WiFi', 1, 991, 1, NULL, '2020-11-10 10:37:49', '2021-01-07 12:21:11');
INSERT INTO `sys_dict_detail` VALUES (1325991038380158978, 1325990867273527297, '无WiFi', '无WiFi', 0, 99, 1, NULL, '2020-11-10 10:37:54', '2020-11-10 10:37:54');
INSERT INTO `sys_dict_detail` VALUES (1325991114011848706, 1325990925045870594, '禁吸烟', '禁吸烟', 1, 99, 1, NULL, '2020-11-10 10:38:12', '2020-12-08 18:17:42');
INSERT INTO `sys_dict_detail` VALUES (1325991126083055618, 1325990925045870594, '可吸烟', '可吸烟', 0, 99, 1, NULL, '2020-11-10 10:38:15', '2020-11-10 10:38:15');
INSERT INTO `sys_dict_detail` VALUES (1326164126631780353, 1326163737501032450, 'Street Parking', 'Street Parking', 0, 99, 1, NULL, '2020-11-10 22:05:41', '2020-11-10 22:05:41');
INSERT INTO `sys_dict_detail` VALUES (1326164357142339586, 1326163737501032450, 'Free Wifi', 'Free Wifi', 0, 99, 1, NULL, '2020-11-10 22:06:36', '2020-11-10 22:06:36');
INSERT INTO `sys_dict_detail` VALUES (1326164377530851329, 1326163737501032450, 'CCTV', 'CCTV', 0, 99, 1, NULL, '2020-11-10 22:06:41', '2020-11-10 22:06:41');
INSERT INTO `sys_dict_detail` VALUES (1326164398527537154, 1326163737501032450, 'Luggage Storage', 'Luggage Storage', 0, 99, 1, NULL, '2020-11-10 22:06:46', '2020-11-10 22:06:46');
INSERT INTO `sys_dict_detail` VALUES (1326164420853817346, 1326163737501032450, 'Laundry Service', 'Laundry Service', 0, 99, 1, NULL, '2020-11-10 22:06:52', '2020-11-10 22:06:52');
INSERT INTO `sys_dict_detail` VALUES (1326164446669758466, 1326163737501032450, '24 Hours Front Desk Service', '24 Hours Front Desk Service', 0, 99, 1, NULL, '2020-11-10 22:06:58', '2020-11-10 22:06:58');
INSERT INTO `sys_dict_detail` VALUES (1326164473190342658, 1326163737501032450, 'Shared lounge', 'Shared lounge', 0, 99, 1, NULL, '2020-11-10 22:07:04', '2020-11-10 22:07:04');
INSERT INTO `sys_dict_detail` VALUES (1326164495218827265, 1326163737501032450, 'Lift / Elevator', 'Lift / Elevator', 0, 99, 1, NULL, '2020-11-10 22:07:09', '2020-11-10 22:07:09');
INSERT INTO `sys_dict_detail` VALUES (1326164516295204866, 1326163737501032450, 'Vending Machines', 'Vending Machines', 0, 99, 1, NULL, '2020-11-10 22:07:14', '2020-11-10 22:07:14');
INSERT INTO `sys_dict_detail` VALUES (1326164539410014210, 1326163737501032450, 'Wheelchair Accessible', 'Wheelchair Accessible', 0, 99, 1, NULL, '2020-11-10 22:07:20', '2020-11-10 22:07:20');
INSERT INTO `sys_dict_detail` VALUES (1326164557936254978, 1326163737501032450, 'Rental service', 'Rental service', 0, 99, 1, NULL, '2020-11-10 22:07:24', '2020-11-10 22:07:24');
INSERT INTO `sys_dict_detail` VALUES (1326164580044431361, 1326163737501032450, 'Daily Housekeeping Service', 'Daily Housekeeping Service', 0, 99, 1, NULL, '2020-11-10 22:07:29', '2020-11-10 22:07:29');
INSERT INTO `sys_dict_detail` VALUES (1326164601309552641, 1326163737501032450, 'Non-smoking Rooms', 'Non-smoking Rooms', 0, 99, 1, NULL, '2020-11-10 22:07:35', '2020-11-10 22:07:35');
INSERT INTO `sys_dict_detail` VALUES (1326164619718352898, 1326163737501032450, 'Family Rooms', 'Family Rooms', 0, 99, 1, NULL, '2020-11-10 22:07:39', '2020-11-10 22:07:39');
INSERT INTO `sys_dict_detail` VALUES (1326164640748593154, 1326163737501032450, 'Room Service', 'Room Service', 0, 99, 1, NULL, '2020-11-10 22:07:44', '2020-11-10 22:07:44');
INSERT INTO `sys_dict_detail` VALUES (1326164663829848065, 1326163737501032450, 'Meeting / Banquet Service', 'Meeting / Banquet Service', 0, 99, 1, NULL, '2020-11-10 22:07:49', '2020-11-10 22:07:49');
INSERT INTO `sys_dict_detail` VALUES (1326165117246693378, 1326162414768545794, 'Air-conditioning', 'Air-conditioning', 0, 99, 1, NULL, '2020-11-10 22:09:38', '2020-11-10 22:09:38');
INSERT INTO `sys_dict_detail` VALUES (1326165136502743042, 1326162414768545794, 'Free Wi-Fi', 'Free Wi-Fi', 0, 99, 1, NULL, '2020-11-10 22:09:42', '2020-11-10 22:09:42');
INSERT INTO `sys_dict_detail` VALUES (1326165154672467969, 1326162414768545794, 'Smoke Detector', 'Smoke Detector', 0, 99, 1, NULL, '2020-11-10 22:09:46', '2020-11-10 22:09:46');
INSERT INTO `sys_dict_detail` VALUES (1326165170963144705, 1326162414768545794, 'Flat Screen TV', 'Flat Screen TV', 0, 99, 1, NULL, '2020-11-10 22:09:50', '2020-11-10 22:09:50');
INSERT INTO `sys_dict_detail` VALUES (1326165196258992130, 1326162414768545794, 'Satellite TV Channels', 'Satellite TV Channels', 0, 99, 1, NULL, '2020-11-10 22:09:56', '2020-11-10 22:09:56');
INSERT INTO `sys_dict_detail` VALUES (1326165213677936642, 1326162414768545794, 'Direct Dial Phone', 'Direct Dial Phone', 0, 99, 1, NULL, '2020-11-10 22:10:01', '2020-11-10 22:10:01');
INSERT INTO `sys_dict_detail` VALUES (1326165231675695106, 1326162414768545794, 'Coffee and Tea Maker', 'Coffee and Tea Maker', 0, 99, 1, NULL, '2020-11-10 22:10:05', '2020-11-10 22:10:05');
INSERT INTO `sys_dict_detail` VALUES (1326165251074351105, 1326162414768545794, 'Tea and Coffee Sachet', 'Tea and Coffee Sachet', 0, 99, 1, NULL, '2020-11-10 22:10:09', '2020-11-10 22:10:09');
INSERT INTO `sys_dict_detail` VALUES (1326165278169554946, 1326162414768545794, 'Bottled Water', 'Bottled Water', 0, 99, 1, NULL, '2020-11-10 22:10:16', '2020-11-10 22:10:16');
INSERT INTO `sys_dict_detail` VALUES (1326165370515546113, 1326162414768545794, 'Electric Kettle', 'Electric Kettle', 0, 99, 1, NULL, '2020-11-10 22:10:38', '2020-11-10 22:10:38');
INSERT INTO `sys_dict_detail` VALUES (1326165390149083138, 1326162414768545794, 'Mini Bar', 'Mini Bar', 0, 99, 1, NULL, '2020-11-10 22:10:43', '2020-11-10 22:10:43');
INSERT INTO `sys_dict_detail` VALUES (1326165408041984001, 1326162414768545794, 'Refrigerator', 'Refrigerator', 0, 99, 1, NULL, '2020-11-10 22:10:47', '2020-11-10 22:10:47');
INSERT INTO `sys_dict_detail` VALUES (1326165424961806338, 1326162414768545794, 'Private Bathroom', 'Private Bathroom', 0, 99, 1, NULL, '2020-11-10 22:10:51', '2020-11-10 22:10:51');
INSERT INTO `sys_dict_detail` VALUES (1326165442200395777, 1326162414768545794, 'Toiletries', 'Toiletries', 0, 99, 1, NULL, '2020-11-10 22:10:55', '2020-11-10 22:10:55');
INSERT INTO `sys_dict_detail` VALUES (1326165460282040321, 1326162414768545794, 'Mirror', 'Mirror', 0, 99, 1, NULL, '2020-11-10 22:10:59', '2020-11-10 22:10:59');
INSERT INTO `sys_dict_detail` VALUES (1326165476992147457, 1326162414768545794, 'Towels', 'Towels', 0, 99, 1, NULL, '2020-11-10 22:11:03', '2020-11-10 22:11:03');
INSERT INTO `sys_dict_detail` VALUES (1326165493136023554, 1326162414768545794, 'Hair Dryer', 'Hair Dryer', 0, 99, 1, NULL, '2020-11-10 22:11:07', '2020-11-10 22:11:07');
INSERT INTO `sys_dict_detail` VALUES (1326165514623442946, 1326162414768545794, 'Room Slippers', 'Room Slippers', 0, 99, 1, NULL, '2020-11-10 22:11:12', '2020-11-10 22:11:12');
INSERT INTO `sys_dict_detail` VALUES (1326167220543401986, 1326167143477260290, '快捷型', '快捷型', 0, 99, 1, NULL, '2020-11-10 22:17:59', '2020-11-10 22:17:59');
INSERT INTO `sys_dict_detail` VALUES (1326167244010532865, 1326167143477260290, '经济型', '经济型', 0, 99, 1, NULL, '2020-11-10 22:18:05', '2020-11-10 22:18:05');
INSERT INTO `sys_dict_detail` VALUES (1326167261949571073, 1326167143477260290, '商务型', '商务型', 0, 99, 1, NULL, '2020-11-10 22:18:09', '2020-11-10 22:18:09');
INSERT INTO `sys_dict_detail` VALUES (1326167279175577601, 1326167143477260290, '度假型', '度假型', 0, 99, 1, NULL, '2020-11-10 22:18:13', '2020-11-10 22:18:13');
INSERT INTO `sys_dict_detail` VALUES (1326167295302676482, 1326167143477260290, '主题型', '主题型', 0, 99, 1, NULL, '2020-11-10 22:18:17', '2020-11-10 22:18:17');
INSERT INTO `sys_dict_detail` VALUES (1326167312390270977, 1326167143477260290, '公寓型', '公寓型', 0, 99, 1, NULL, '2020-11-10 22:18:21', '2020-11-10 22:18:21');
INSERT INTO `sys_dict_detail` VALUES (1326167330358669313, 1326167143477260290, '会议型', '会议型', 0, 99, 1, NULL, '2020-11-10 22:18:25', '2020-11-10 22:18:25');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `pid` bigint(0) NULL DEFAULT NULL COMMENT '关联',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `size` bigint(0) NULL DEFAULT NULL COMMENT '大小',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存放相对路径',
  `text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `href` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '点击后跳转URL',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` int(0) NULL DEFAULT 1 COMMENT '日志类型：1->登录/登出，2->业务，3->异常',
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `remote_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '日志标题',
  `request_uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `params` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求数据',
  `user_agent` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户代理',
  `time` bigint(0) NULL DEFAULT NULL COMMENT '执行时间',
  `class_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行对象',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行方法',
  `exception` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  `ctime` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sys_log_create_by`(`username`) USING BTREE,
  INDEX `sys_log_request_uri`(`request_uri`) USING BTREE,
  INDEX `sys_log_type`(`type`) USING BTREE,
  INDEX `sys_log_create_date`(`ctime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1120 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统-日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1019, 1, 'admin', '127.0.0.1', '用户登出', '/system/sysuser/logout', 'GET', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-16 21:36:35', '2021-01-16 21:36:35');
INSERT INTO `sys_log` VALUES (1020, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-16 21:36:37', '2021-01-16 21:36:37');
INSERT INTO `sys_log` VALUES (1021, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 15, NULL, NULL, NULL, '2021-01-16 21:38:23', '2021-01-16 21:38:23');
INSERT INTO `sys_log` VALUES (1022, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 17, NULL, NULL, NULL, '2021-01-16 21:41:42', '2021-01-16 21:41:42');
INSERT INTO `sys_log` VALUES (1023, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 17, NULL, NULL, NULL, '2021-01-16 21:44:56', '2021-01-16 21:44:56');
INSERT INTO `sys_log` VALUES (1024, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 6, NULL, NULL, NULL, '2021-01-16 21:45:15', '2021-01-16 21:45:15');
INSERT INTO `sys_log` VALUES (1025, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 9, NULL, NULL, NULL, '2021-01-16 21:57:59', '2021-01-16 21:57:59');
INSERT INTO `sys_log` VALUES (1026, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 8, NULL, NULL, NULL, '2021-01-16 21:58:04', '2021-01-16 21:58:04');
INSERT INTO `sys_log` VALUES (1027, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 6, NULL, NULL, NULL, '2021-01-16 21:59:13', '2021-01-16 21:59:13');
INSERT INTO `sys_log` VALUES (1028, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 5, NULL, NULL, NULL, '2021-01-16 21:59:25', '2021-01-16 21:59:25');
INSERT INTO `sys_log` VALUES (1029, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 8, NULL, NULL, NULL, '2021-01-16 21:59:34', '2021-01-16 21:59:34');
INSERT INTO `sys_log` VALUES (1030, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 6, NULL, NULL, NULL, '2021-01-16 21:59:36', '2021-01-16 21:59:36');
INSERT INTO `sys_log` VALUES (1031, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 6, NULL, NULL, NULL, '2021-01-16 22:01:15', '2021-01-16 22:01:15');
INSERT INTO `sys_log` VALUES (1032, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-16 22:10:37', '2021-01-16 22:10:37');
INSERT INTO `sys_log` VALUES (1033, 3, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 6, NULL, NULL, NULL, '2021-01-16 22:10:48', '2021-01-16 22:10:48');
INSERT INTO `sys_log` VALUES (1034, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-16 23:09:02', '2021-01-16 23:09:02');
INSERT INTO `sys_log` VALUES (1035, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-17 12:10:11', '2021-01-17 12:10:11');
INSERT INTO `sys_log` VALUES (1036, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-17 13:46:34', '2021-01-17 13:46:34');
INSERT INTO `sys_log` VALUES (1037, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 13:56:34', '2021-01-17 13:56:34');
INSERT INTO `sys_log` VALUES (1038, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 13:57:10', '2021-01-17 13:57:10');
INSERT INTO `sys_log` VALUES (1039, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:01:25', '2021-01-17 14:01:25');
INSERT INTO `sys_log` VALUES (1040, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:07:45', '2021-01-17 14:07:45');
INSERT INTO `sys_log` VALUES (1041, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:13:26', '2021-01-17 14:13:26');
INSERT INTO `sys_log` VALUES (1042, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:14:59', '2021-01-17 14:14:59');
INSERT INTO `sys_log` VALUES (1043, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D&keyword=%5Baaaaaaaa%5D&status=%5B2%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:15:49', '2021-01-17 14:15:49');
INSERT INTO `sys_log` VALUES (1044, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', 'page=%5B1%5D&limit=%5B10%5D&keyword=%5Baaaaaaaa%5D&status=%5B2%5D', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:19:58', '2021-01-17 14:19:58');
INSERT INTO `sys_log` VALUES (1045, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"keyword\":\"aaaaaaaa\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:21:31', '2021-01-17 14:21:31');
INSERT INTO `sys_log` VALUES (1046, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:22:03', '2021-01-17 14:22:03');
INSERT INTO `sys_log` VALUES (1047, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:23:55', '2021-01-17 14:23:55');
INSERT INTO `sys_log` VALUES (1048, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:25:58', '2021-01-17 14:25:58');
INSERT INTO `sys_log` VALUES (1049, 4, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, '/ by zero', '2021-01-17 14:26:00', '2021-01-17 14:26:00');
INSERT INTO `sys_log` VALUES (1050, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', NULL, NULL, NULL, NULL, '2021-01-17 19:06:12', '2021-01-17 19:06:12');
INSERT INTO `sys_log` VALUES (1051, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 06:52:56', '2021-02-04 06:52:56');
INSERT INTO `sys_log` VALUES (1052, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 134, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 06:53:23', '2021-02-04 06:53:23');
INSERT INTO `sys_log` VALUES (1053, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 41, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 06:56:31', '2021-02-04 06:56:31');
INSERT INTO `sys_log` VALUES (1054, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 10:13:13', '2021-02-04 10:13:13');
INSERT INTO `sys_log` VALUES (1055, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 35, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 10:13:34', '2021-02-04 10:13:34');
INSERT INTO `sys_log` VALUES (1056, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 41, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 10:14:01', '2021-02-04 10:14:01');
INSERT INTO `sys_log` VALUES (1057, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 11:58:43', '2021-02-04 11:58:43');
INSERT INTO `sys_log` VALUES (1058, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 16:13:17', '2021-02-04 16:13:17');
INSERT INTO `sys_log` VALUES (1059, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 16:50:01', '2021-02-04 16:50:01');
INSERT INTO `sys_log` VALUES (1060, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 42, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:50:04', '2021-02-04 16:50:04');
INSERT INTO `sys_log` VALUES (1061, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 39, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:50:16', '2021-02-04 16:50:16');
INSERT INTO `sys_log` VALUES (1062, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 37, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:50:55', '2021-02-04 16:50:55');
INSERT INTO `sys_log` VALUES (1063, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 43, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:51:24', '2021-02-04 16:51:24');
INSERT INTO `sys_log` VALUES (1064, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 54, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:53:20', '2021-02-04 16:53:20');
INSERT INTO `sys_log` VALUES (1065, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 41, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:55:02', '2021-02-04 16:55:02');
INSERT INTO `sys_log` VALUES (1066, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 53, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:56:23', '2021-02-04 16:56:23');
INSERT INTO `sys_log` VALUES (1067, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 5, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:56:45', '2021-02-04 16:56:45');
INSERT INTO `sys_log` VALUES (1068, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 5, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:56:58', '2021-02-04 16:56:58');
INSERT INTO `sys_log` VALUES (1069, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 44, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:57:12', '2021-02-04 16:57:12');
INSERT INTO `sys_log` VALUES (1070, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 6, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:57:36', '2021-02-04 16:57:36');
INSERT INTO `sys_log` VALUES (1071, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 37, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:58:05', '2021-02-04 16:58:05');
INSERT INTO `sys_log` VALUES (1072, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 4, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:58:07', '2021-02-04 16:58:07');
INSERT INTO `sys_log` VALUES (1073, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 38, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:58:24', '2021-02-04 16:58:24');
INSERT INTO `sys_log` VALUES (1074, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 3, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:58:26', '2021-02-04 16:58:26');
INSERT INTO `sys_log` VALUES (1075, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 39, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:58:58', '2021-02-04 16:58:58');
INSERT INTO `sys_log` VALUES (1076, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 100, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:04', '2021-02-04 16:59:04');
INSERT INTO `sys_log` VALUES (1077, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 6, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:06', '2021-02-04 16:59:06');
INSERT INTO `sys_log` VALUES (1078, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 31, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:10', '2021-02-04 16:59:10');
INSERT INTO `sys_log` VALUES (1079, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 30, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:16', '2021-02-04 16:59:16');
INSERT INTO `sys_log` VALUES (1080, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\",\"status\":\"2\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 3, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:23', '2021-02-04 16:59:23');
INSERT INTO `sys_log` VALUES (1081, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 30, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 16:59:26', '2021-02-04 16:59:26');
INSERT INTO `sys_log` VALUES (1082, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 37, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:00:52', '2021-02-04 17:00:52');
INSERT INTO `sys_log` VALUES (1083, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 36, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:02:38', '2021-02-04 17:02:38');
INSERT INTO `sys_log` VALUES (1084, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 39, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:03:23', '2021-02-04 17:03:23');
INSERT INTO `sys_log` VALUES (1085, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 39, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:06:37', '2021-02-04 17:06:37');
INSERT INTO `sys_log` VALUES (1086, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 25, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:09:10', '2021-02-04 17:09:10');
INSERT INTO `sys_log` VALUES (1087, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 49, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:20:05', '2021-02-04 17:20:05');
INSERT INTO `sys_log` VALUES (1088, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 61, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:20:14', '2021-02-04 17:20:14');
INSERT INTO `sys_log` VALUES (1089, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 61, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:21:29', '2021-02-04 17:21:29');
INSERT INTO `sys_log` VALUES (1090, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 37, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:22:23', '2021-02-04 17:22:23');
INSERT INTO `sys_log` VALUES (1091, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 46, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:22:49', '2021-02-04 17:22:49');
INSERT INTO `sys_log` VALUES (1092, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 40, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:24:51', '2021-02-04 17:24:51');
INSERT INTO `sys_log` VALUES (1093, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 47, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:25:08', '2021-02-04 17:25:08');
INSERT INTO `sys_log` VALUES (1094, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 24, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:26:38', '2021-02-04 17:26:38');
INSERT INTO `sys_log` VALUES (1095, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 32, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 17:27:02', '2021-02-04 17:27:02');
INSERT INTO `sys_log` VALUES (1096, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 21:14:21', '2021-02-04 21:14:21');
INSERT INTO `sys_log` VALUES (1097, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 71, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:18:21', '2021-02-04 21:18:21');
INSERT INTO `sys_log` VALUES (1098, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 33, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:47:08', '2021-02-04 21:47:08');
INSERT INTO `sys_log` VALUES (1099, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 30, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:47:32', '2021-02-04 21:47:32');
INSERT INTO `sys_log` VALUES (1100, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 51, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:47:47', '2021-02-04 21:47:47');
INSERT INTO `sys_log` VALUES (1101, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 22, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:50:08', '2021-02-04 21:50:08');
INSERT INTO `sys_log` VALUES (1102, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 22, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:50:22', '2021-02-04 21:50:22');
INSERT INTO `sys_log` VALUES (1103, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 29, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:50:33', '2021-02-04 21:50:33');
INSERT INTO `sys_log` VALUES (1104, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 32, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 21:50:39', '2021-02-04 21:50:39');
INSERT INTO `sys_log` VALUES (1105, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 21:55:52', '2021-02-04 21:55:52');
INSERT INTO `sys_log` VALUES (1106, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 38, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:00:01', '2021-02-04 22:00:01');
INSERT INTO `sys_log` VALUES (1107, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 31, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:10:52', '2021-02-04 22:10:52');
INSERT INTO `sys_log` VALUES (1108, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 41, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:12:50', '2021-02-04 22:12:50');
INSERT INTO `sys_log` VALUES (1109, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 43, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:19:57', '2021-02-04 22:19:57');
INSERT INTO `sys_log` VALUES (1110, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 33, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:21:53', '2021-02-04 22:21:53');
INSERT INTO `sys_log` VALUES (1111, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 22:22:26', '2021-02-04 22:22:26');
INSERT INTO `sys_log` VALUES (1112, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 22:22:51', '2021-02-04 22:22:51');
INSERT INTO `sys_log` VALUES (1113, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 29, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:23:39', '2021-02-04 22:23:39');
INSERT INTO `sys_log` VALUES (1114, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 35, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:23:45', '2021-02-04 22:23:45');
INSERT INTO `sys_log` VALUES (1115, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 44, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:33:34', '2021-02-04 22:33:34');
INSERT INTO `sys_log` VALUES (1116, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 23, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-04 22:51:59', '2021-02-04 22:51:59');
INSERT INTO `sys_log` VALUES (1117, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 22:55:25', '2021-02-04 22:55:25');
INSERT INTO `sys_log` VALUES (1118, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 23:34:16', '2021-02-04 23:34:16');
INSERT INTO `sys_log` VALUES (1119, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-04 23:59:41', '2021-02-04 23:59:41');
INSERT INTO `sys_log` VALUES (1120, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-05 21:15:30', '2021-02-05 21:15:30');
INSERT INTO `sys_log` VALUES (1121, 1, 'admin', '127.0.0.1', '用户登录', '/system/sysuser/login', 'POST', '{}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', NULL, NULL, NULL, NULL, '2021-02-06 09:41:27', '2021-02-06 09:41:27');
INSERT INTO `sys_log` VALUES (1122, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 69, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-06 09:43:18', '2021-02-06 09:43:18');
INSERT INTO `sys_log` VALUES (1123, 2, 'admin', '127.0.0.1', '查询系统用户列表', '/system/sysuser/page', 'GET', '{\"limit\":\"10\",\"page\":\"1\"}', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 42, 'com.quan.system.web.controller.SysUserController', 'queryPage', NULL, '2021-02-06 09:43:22', '2021-02-06 09:43:22');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` bigint(0) NULL DEFAULT 0 COMMENT 'PID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `sort` int(0) NOT NULL DEFAULT 99 COMMENT '排序',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态(0:禁用:,1:正常)',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (60, 0, '系统管理', '/system', 99, 1, '2021-01-09 23:14:19', '2021-01-09 23:14:19');
INSERT INTO `sys_menu` VALUES (62, 60, '用户管理', '/system/user', 99, 1, '2021-01-09 23:16:20', '2021-01-09 23:16:20');
INSERT INTO `sys_menu` VALUES (63, 60, '角色管理', '/system/role', 99, 1, '2021-01-09 23:16:30', '2021-01-09 23:16:30');
INSERT INTO `sys_menu` VALUES (64, 60, '字典管理', '/system/dict', 99, 1, '2021-01-09 23:16:38', '2021-01-09 23:16:38');
INSERT INTO `sys_menu` VALUES (65, 60, '代码生成', '/system/generator', 99, 1, '2021-01-09 23:16:48', '2021-01-09 23:16:48');
INSERT INTO `sys_menu` VALUES (66, 60, '权限管理', '/system/permission', 99, 1, '2021-01-10 00:22:44', '2021-01-10 00:22:44');

-- ----------------------------
-- Table structure for sys_message
-- ----------------------------
DROP TABLE IF EXISTS `sys_message`;
CREATE TABLE `sys_message`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `type` tinyint(1) NULL DEFAULT 1 COMMENT '消息类型：1->广播消息，2->用户消息',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '消息状态：1->未读，2->已读',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '消息内容',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-即时消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_message_to_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_message_to_user`;
CREATE TABLE `sys_message_to_user`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `message_id` bigint(0) NOT NULL COMMENT '消息ID',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '消息状态：1->未读，2->已读',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-即时消息-用户消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_id` int(0) NULL DEFAULT NULL COMMENT '所属菜单ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `permission` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限类型 （1:页面,2:按钮）',
  `sort` int(0) NOT NULL DEFAULT 99 COMMENT '排序',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态(0:禁用:,1:正常)',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-菜单权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (173, 60, '系统管理页面', 'system:page', '1', 99, 1, NULL, '2021-01-09 23:17:47', '2021-01-09 23:18:00');
INSERT INTO `sys_permission` VALUES (174, 62, '页面', 'system:user:page', '1', 99, 1, NULL, '2021-01-09 23:18:23', '2021-01-09 23:18:23');
INSERT INTO `sys_permission` VALUES (175, 62, '添加', 'system:user:add', '2', 99, 1, NULL, '2021-01-09 23:18:45', '2021-01-09 23:18:45');
INSERT INTO `sys_permission` VALUES (176, 62, '删除', 'system:user:delete', '2', 99, 1, NULL, '2021-01-09 23:18:52', '2021-01-09 23:18:52');
INSERT INTO `sys_permission` VALUES (177, 62, '编辑', 'system:user:update', '2', 99, 1, NULL, '2021-01-09 23:19:05', '2021-01-09 23:19:05');
INSERT INTO `sys_permission` VALUES (179, 62, '详情', 'system:user:detail', '2', 99, 1, NULL, '2021-01-09 23:20:50', '2021-01-09 23:20:50');
INSERT INTO `sys_permission` VALUES (180, 63, '页面', 'system:role:page', '1', 99, 1, NULL, '2021-01-09 23:21:11', '2021-01-09 23:21:11');
INSERT INTO `sys_permission` VALUES (181, 63, '添加', 'system:role:add', '2', 99, 1, NULL, '2021-01-09 23:21:18', '2021-01-09 23:21:18');
INSERT INTO `sys_permission` VALUES (182, 63, '删除', 'system:role:delete', '2', 99, 1, NULL, '2021-01-09 23:21:28', '2021-01-09 23:21:28');
INSERT INTO `sys_permission` VALUES (183, 63, '编辑', 'system:role:edit', '2', 99, 1, NULL, '2021-01-09 23:21:41', '2021-01-09 23:21:41');
INSERT INTO `sys_permission` VALUES (184, 63, '详情', 'system:role:detail', '2', 99, 1, NULL, '2021-01-09 23:21:52', '2021-01-09 23:21:52');
INSERT INTO `sys_permission` VALUES (185, 63, '查询列表', 'system:role:list', '2', 99, 1, NULL, '2021-01-09 23:22:09', '2021-01-09 23:22:09');
INSERT INTO `sys_permission` VALUES (186, 62, '查询列表', 'system:user:list', '2', 99, 1, NULL, '2021-01-09 23:22:38', '2021-01-09 23:22:38');
INSERT INTO `sys_permission` VALUES (187, 64, '页面', 'system:dict:page', '1', 99, 1, NULL, '2021-01-09 23:23:05', '2021-01-09 23:23:05');
INSERT INTO `sys_permission` VALUES (188, 64, '添加', 'system:dict:add', '2', 99, 1, NULL, '2021-01-09 23:23:18', '2021-01-09 23:23:18');
INSERT INTO `sys_permission` VALUES (189, 64, '删除', 'system:dict:delete', '2', 99, 1, NULL, '2021-01-09 23:23:26', '2021-01-09 23:23:26');
INSERT INTO `sys_permission` VALUES (190, 64, '编辑', 'system:dict:edit', '2', 99, 1, NULL, '2021-01-09 23:23:33', '2021-01-09 23:23:33');
INSERT INTO `sys_permission` VALUES (191, 64, '详情', 'system:dict:detail', '2', 99, 1, NULL, '2021-01-09 23:23:40', '2021-01-09 23:23:40');
INSERT INTO `sys_permission` VALUES (192, 64, '查询列表', 'system:dict:list', '2', 99, 1, NULL, '2021-01-09 23:23:45', '2021-01-09 23:23:45');
INSERT INTO `sys_permission` VALUES (193, 65, '页面', 'system:generator:page', '1', 99, 1, NULL, '2021-01-09 23:24:19', '2021-01-09 23:24:43');
INSERT INTO `sys_permission` VALUES (194, 66, '页面', 'system:permission:page', '1', 99, 1, NULL, '2021-01-10 00:23:11', '2021-01-10 00:23:11');
INSERT INTO `sys_permission` VALUES (195, 66, '添加', 'system:permission:add', '2', 99, 1, NULL, '2021-01-10 00:23:18', '2021-01-10 00:23:18');
INSERT INTO `sys_permission` VALUES (196, 66, '删除', 'system:permission:delete', '2', 99, 1, NULL, '2021-01-10 00:23:24', '2021-01-10 00:23:24');
INSERT INTO `sys_permission` VALUES (197, 66, '编辑', 'system:permission:edit', '2', 99, 1, NULL, '2021-01-10 00:23:29', '2021-01-10 00:23:29');
INSERT INTO `sys_permission` VALUES (198, 66, '详情', 'system:permission:detail', '2', 99, 1, NULL, '2021-01-10 00:23:43', '2021-01-10 00:23:43');
INSERT INTO `sys_permission` VALUES (199, 66, '查询列表', 'system:permission:list', '2', 99, 1, NULL, '2021-01-10 00:23:52', '2021-01-10 00:23:52');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1347928124964237314, '管理员', NULL, '2021-01-09 23:28:03', '2021-01-09 23:28:03');
INSERT INTO `sys_role` VALUES (1347928219227025409, '店长', NULL, '2021-01-09 23:28:25', '2021-01-09 23:28:25');
INSERT INTO `sys_role` VALUES (1347928296645488641, '财务专员', NULL, '2021-01-09 23:28:44', '2021-01-09 23:28:44');
INSERT INTO `sys_role` VALUES (1347928359996256257, '客服专员', NULL, '2021-01-09 23:28:59', '2021-01-09 23:28:59');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `permission_id` bigint(0) NOT NULL COMMENT '权限ID',
  `menu_id` bigint(0) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-角色与(菜单)权限关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1347940208238145538, 1347928124964237314, 173, 60);
INSERT INTO `sys_role_permission` VALUES (1347940208238145539, 1347928124964237314, 174, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208238145540, 1347928124964237314, 175, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208246534146, 1347928124964237314, 176, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208246534147, 1347928124964237314, 177, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208246534148, 1347928124964237314, 179, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208246534149, 1347928124964237314, 180, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534150, 1347928124964237314, 181, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534151, 1347928124964237314, 182, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534152, 1347928124964237314, 183, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534153, 1347928124964237314, 184, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534154, 1347928124964237314, 185, 63);
INSERT INTO `sys_role_permission` VALUES (1347940208246534155, 1347928124964237314, 186, 62);
INSERT INTO `sys_role_permission` VALUES (1347940208254922753, 1347928124964237314, 187, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922754, 1347928124964237314, 188, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922755, 1347928124964237314, 189, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922756, 1347928124964237314, 190, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922757, 1347928124964237314, 191, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922758, 1347928124964237314, 192, 64);
INSERT INTO `sys_role_permission` VALUES (1347940208254922759, 1347928124964237314, 193, 65);
INSERT INTO `sys_role_permission` VALUES (1347940231260680194, 1347928219227025409, 174, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231264874497, 1347928219227025409, 175, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231264874498, 1347928219227025409, 176, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231264874499, 1347928219227025409, 177, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231264874500, 1347928219227025409, 179, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231273263105, 1347928219227025409, 186, 62);
INSERT INTO `sys_role_permission` VALUES (1347940231273263106, 1347928219227025409, 173, 60);
INSERT INTO `sys_role_permission` VALUES (1347940273799311361, 1347928296645488641, 180, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273807699970, 1347928296645488641, 181, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273807699971, 1347928296645488641, 182, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273807699972, 1347928296645488641, 183, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273807699973, 1347928296645488641, 184, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273807699974, 1347928296645488641, 185, 63);
INSERT INTO `sys_role_permission` VALUES (1347940273816088577, 1347928296645488641, 193, 65);
INSERT INTO `sys_role_permission` VALUES (1347940273816088578, 1347928296645488641, 173, 60);
INSERT INTO `sys_role_permission` VALUES (1347945881248215042, 1347928359996256257, 193, 65);
INSERT INTO `sys_role_permission` VALUES (1347945881248215043, 1347928359996256257, 173, 60);
INSERT INTO `sys_role_permission` VALUES (1347945881248215044, 1347928359996256257, 187, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881248215045, 1347928359996256257, 188, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881264992258, 1347928359996256257, 189, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881264992259, 1347928359996256257, 190, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881264992260, 1347928359996256257, 191, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881264992261, 1347928359996256257, 192, 64);
INSERT INTO `sys_role_permission` VALUES (1347945881264992262, 1347928359996256257, 180, 63);
INSERT INTO `sys_role_permission` VALUES (1347945881264992263, 1347928359996256257, 176, 62);
INSERT INTO `sys_role_permission` VALUES (1347945881264992264, 1347928359996256257, 177, 62);
INSERT INTO `sys_role_permission` VALUES (1347945881264992265, 1347928359996256257, 179, 62);
INSERT INTO `sys_role_permission` VALUES (1347945881264992266, 1347928359996256257, 186, 62);
INSERT INTO `sys_role_permission` VALUES (1347945881264992267, 1347928359996256257, 174, 62);

-- ----------------------------
-- Table structure for sys_scheduler_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_scheduler_job`;
CREATE TABLE `sys_scheduler_job`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `job_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Job名称',
  `job_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Job组名',
  `job_cron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行的cron',
  `job_trigger_date` datetime(0) NULL DEFAULT NULL COMMENT '任务触发时间',
  `job_class_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Job作业类',
  `job_params` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Job的参数',
  `job_desc` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Job描述信息',
  `job_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务执行状态，可选值为（RUNNING：正常-待触发执行中,  PAUSE：暂停, FINISHED：完成, FAILED：失败）',
  `failure_cause` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行失败原因',
  `remarks` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-定时作业管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_scheduler_job
-- ----------------------------
INSERT INTO `sys_scheduler_job` VALUES (4, '测试定时任务', 'DEFAULT', '0/5 * * * * ? *', NULL, 'com.quan.system.commons.job.PushMsgToClientJob', '[{\"key\":\"aa\",\"value\":\"aa\"},{\"key\":\"cc\",\"value\":\"cc\"}]', '111111', 'PAUSE', NULL, '22222', '2021-02-05 22:47:42', '2021-02-05 22:55:39');

-- ----------------------------
-- Table structure for sys_settings
-- ----------------------------
DROP TABLE IF EXISTS `sys_settings`;
CREATE TABLE `sys_settings`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `set_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '全局参数键',
  `set_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '全局参数值',
  `remarks` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-全局常量设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_settings
-- ----------------------------
INSERT INTO `sys_settings` VALUES (58, 'Year_Holiday', '7', '员工年假默认天数');
INSERT INTO `sys_settings` VALUES (59, 'Dimission_Year', '2016', '离职率计算参数设定');
INSERT INTO `sys_settings` VALUES (63, 'ZZRSCE2018', '23', '2018在职人数差额');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `realname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `avatar` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码(SHA256(密码+盐))',
  `salt` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '盐',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别(男,女)',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态(0:禁用:,1:正常)',
  `super_admin` tinyint(1) NULL DEFAULT 0 COMMENT '是否超级管理员（1：是，0：否）',
  `ctime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `utime` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tb_system_user_username`(`username`) USING BTREE,
  INDEX `tb_system_user_mobile`(`mobile`) USING BTREE,
  INDEX `tb_system_user_email`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (290086422317127680, '超级管理员', 'http://yhaoquan-mars.oss-cn-shenzhen.aliyuncs.com/u3urLb', 'admin', '3c9c721aa1f03e78d605ecd60bd0425f983b2aa45372153015206197c2b52526', '4148362425D14D5DBA41EFD72BD83DC7', '15602208451', 'yhaoquan@163.com', '男', '2021-01-06', 1, 1, '2018-05-13 19:21:23', '2021-02-04 22:22:45');
INSERT INTO `sys_user` VALUES (1297776930242228226, '张三-店长', 'https://quan-cloud-test.oss-cn-shenzhen.aliyuncs.com/2021-01-08/9e5740cd-afba-4a70-8477-eb726dfc3afa_220j0s000000hiebj23B5_W_1600_1200_Q70.jpg', '0001', 'de57f13fef49a32f3b7c3c615eeb9cb51decc1fef9bf2d66c1209abb216a24f7', '3A227556CCCE4BA697F4CC6B1F0A23BB', '18526361704', '18526361704@qq.com', '男', '2021-01-05', 1, 1, '2020-08-24 14:05:06', '2021-02-04 21:50:51');
INSERT INTO `sys_user` VALUES (1297777107707424770, '狗蛋', 'https://quan-cloud-test.oss-cn-shenzhen.aliyuncs.com/2021-01-10/3e2a6cff-5cc1-4929-83d3-9da274cad60a_220t0u000000jbdrxACC2_R_300_225.jpg', '0002', 'a3c27e3bec32197f377f4d1c648caa46102bede049c82c4c1d714db288ce0200', '7A9CF5F31B1646588FE48FC8FE5EE94A', '15602221116', '156022211166@qq.com', '男', '2021-01-13', 1, 0, '2020-08-24 14:05:49', '2021-01-16 21:45:05');
INSERT INTO `sys_user` VALUES (1297777107707424771, '狗蛋-分店01-员工', 'https://quan-cloud-test.oss-cn-shenzhen.aliyuncs.com/2021-01-09/d05c685e-43a9-423a-8a48-f93f4a4b25c9_020151200001j2guh0A02_R_300_225.jpg', '0003', 'a3c27e3bec32197f377f4d1c648caa46102bede049c82c4c1d714db288ce0200', '7A9CF5F31B1646588FE48FC8FE5EE94A', '15602221116', '156022211166@qq.com', '男', '2021-01-06', 1, 0, '2020-08-24 14:05:49', '2021-01-10 22:37:35');
INSERT INTO `sys_user` VALUES (1297777107707424772, '狗蛋-分店02', 'https://quan-cloud-test.oss-cn-shenzhen.aliyuncs.com/2021-01-09/71815412-3892-4df7-8a70-832678af361e_0221512000482bfyyC687_R_300_225.jpg', '0004', 'a3c27e3bec32197f377f4d1c648caa46102bede049c82c4c1d714db288ce0200', '7A9CF5F31B1646588FE48FC8FE5EE94A', '15602221116', '156022211166@qq.com', '男', '2021-01-13', 1, 0, '2020-08-24 14:05:49', '2021-01-09 23:30:09');
INSERT INTO `sys_user` VALUES (1297777107707424773, '二狗子', 'https://quan-cloud-test.oss-cn-shenzhen.aliyuncs.com/2021-01-09/fb3bfb67-6319-4754-8312-85e7e7fbe542_0205g1200001j04dfA627_R_300_225.jpg', '0005', 'a3c27e3bec32197f377f4d1c648caa46102bede049c82c4c1d714db288ce0200', '7A9CF5F31B1646588FE48FC8FE5EE94A', '15602221116', '156022211166@qq.com', '男', '2020-12-16', 1, 0, '2020-08-24 14:05:49', '2021-01-10 22:37:36');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(0) NOT NULL COMMENT 'ID',
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-用户与角色关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1347928638774865922, 1297777107707424771, 1347928219227025409);
INSERT INTO `sys_user_role` VALUES (1347928653186494466, 1297777107707424772, 1347928359996256257);
INSERT INTO `sys_user_role` VALUES (1348272299941351426, 1297777107707424770, 1347928359996256257);
INSERT INTO `sys_user_role` VALUES (1348272299941351427, 1297777107707424770, 1347928124964237314);
INSERT INTO `sys_user_role` VALUES (1348273485775949825, 1297776930242228226, 1347928124964237314);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(0) NOT NULL,
  `xid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int(0) NOT NULL,
  `log_created` datetime(0) NOT NULL,
  `log_modified` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
